---
# SPDX-FileCopyrightText: 2022-2023 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Boreal Manager"
weight: 2
---

## Introduction
The Boreal workflow is centrally managed by the `Boreal Manager`, an automation script designed to streamline project configuration and the build process for Caribou users. It provides a command-line interface that guides users through the entire build workflow, allowing them to target specific devices and boards efficiently.

```shell
usage: boreal-manager.py [-h] {list,create,build} ...

Boreal Vivado project manager

positional arguments:
  {list,create,build}  Sub-command help
    list               List available devices
    create             Create Vivado project
    build              Build Vivado project

optional arguments:
  -h, --help           show this help message and exit
```

### List
The `list` command displays all devices integrated into Boreal, along with the supported boards.

```shell
usage: boreal-manager.py list [-h]

optional arguments:
  -h, --help  show this help message and exit
```

### Create
The `create` command generates a Vivado project for a selected device and its corresponding board.

```shell
usage: boreal-manager.py create [-h] [--device device_name] [--board board_name]

optional arguments:
  -h, --help            show this help message and exit
  --device device_name  Name of the device
  --board board_name    Name of the board
```

- `--device`: Specifies the target device
- `--board`: Specifies the target board

### Build
The `build` command launches synthesis, implementation as well as bitstream (`*.bit`) and hardware description (`*.xsa`) files creation.

```shell
usage: boreal-manager.py build [-h] [--device device_name] [--board board_name]

optional arguments:
  -h, --help            show this help message and exit
  --device device_name  Name of the device
  --board board_name    Name of the project
```

- `--device`: Specifies the target device
- `--board`: Specifies the target board
