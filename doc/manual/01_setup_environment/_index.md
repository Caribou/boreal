---
# SPDX-FileCopyrightText: 2022 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Setup Environment"
weight: 1
---

## Requirements
- **Vivado:** 2023.2
- **Python:** 3.*
- **Cocotb:** any version

## Installing Vivado Design Suite
Before getting started, ensure that Vivado is installed. This tool, provided by AMD-Xilinx for their FPGA and SoC products, must be downloaded from the AMD-Xilinx [downloads portal](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/2023-2.html). A registered AMD-Xilinx account is required for access.

Boreal uses Vivado 2023.2, as this version is the last to support the ZC706 evaluation board through its corresponding PetaLinux release.

## About simulation tools
Boreal does not mandate a specific simulation tool, allowing users to choose their preferred option. However, to accelerate the design cycle, [Cocotb](https://www.cocotb.org) has proven to be an excellent choice for developing simulation testbenches. Cocotb is an open-source, coroutine-based co-simulation framework that enables verification of VHDL and SystemVerilog RTL using Python in conjunction with various simulation tools.