---
# SPDX-FileCopyrightText: 2022 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Boreal Firmware"
description: "The Caribou FPGA design framework"
weight: 4
---

## Introduction

The Boreal project is the unified FPGA design framework for the Caribou base system and all its supported devices. It offers a common development environment consolidated that simplifies the integration of new devices accross multiple platforms. Boreal provides various commercial or custom IPs, project management tools and a streamlined development workflow.

The firmware name derives from the North American subspecies of reindeer, the Boreal woodland caribou, found primarily in Canada [(Wikipedia)](https://en.wikipedia.org/wiki/Boreal_woodland_caribou)

## Source Code, Support and Reporting Issues

The source code is hosted on the CERN Gitlab instance in the [boreal repository](https://gitlab.cern.ch/Caribou/boreal). Please note that only limited support on best-effort basis for this firmware infrastructure can be offered. The authors are however happy to receive feedback on potential improvements or problems arising. Reports on issues, questions concerning the infrastructure as well as documentation and potential improvements are very much appreciated. All issues and questions should be reported on the Caribou project [issue tracker](https://gitlab.cern.ch/Caribou/boreal/-/issues).

## Contributing

Caribou Boreal is a community project that benefits from active participation in the development and code contribution from users. Any person interested in joining the effort is encouraged to contact the core developers at *caribou-developers@cern.ch*. In addition users and prospective developers are welcome to discuss their needs via the issue tracker in order to receive ideas and guidance on how to integrate their device or implement specific features. In fact, getting in touch with the core developers early in the development cycle avoids spending time on features which already exist or are currently under development by others.
