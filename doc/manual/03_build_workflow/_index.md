---
# SPDX-FileCopyrightText: 2022-2023 CERN and the Caribou authors
# SPDX-License-Identifier: CC-BY-4.0
title: "Build Workflow"
weight: 3
---

The build workflow of Boreal is very well streamlined and consists of two main steps:

1. Project creation
2. Project building

This section provides a step-by-step guide to the build workflow using the [Boreal Manager](https://caribou-project.docs.cern.ch/docs/03_boreal_firmware/02_boreal_manager/). The workflow's final output is a bitstream and hardware description file tailored to a specific device and board combination.

### **1. Create project for target device and board**
```shell
$ ./boreal-manager.py create --device <device-name> --board <board-name>
```

This step creates the Vivado project of the target device and board.

### **2. Build project**
```shell
$ ./boreal-manager.py build --device <device-name> --board <board-name>
```

This step initiates the synthesis, implementation, and bitstream (`*.bit`) generation for the specified device and board. Additionally, it produces a hardware description file (`*.xsa`). The `*.bit` and `*.xsa` files can be found inside the implementation output folder at `<path-to-project/<project-name>.runs/impl_1`.