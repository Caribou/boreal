## Local Structure

### <ins> dev </ins>
Developper documentation. This includes all files that do not belong to the firmware project but that might be needed by the developper in the proccess of contributing to the firmware (ie: caribou signal mapping, evaluation board example constraints, ...)

### <ins> usr </ins>
User documentation. This includes documentation dedicated to Boreal firmware users and developpers. It is intended to be deployed on the Caribou project website. It contains all information and guidelines related to the Boreal firmware configurations and functionalities as well as user block integration.
