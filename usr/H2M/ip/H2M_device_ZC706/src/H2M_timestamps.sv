`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date:
// Design Name:
// Module Name:
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////

// signal clock domain crossing
module H2M_timestamps #(
    byte unsigned TS_WIDTH = 48,
    byte unsigned FRAME_NO_WIDTH = 32
  )(
    input  daq_clk,
    input  axi_clk,
    input  arst,
    input  t0_async,
    input  shutter_syncdaq,
    input  trigger_syncdaq,
    input  conf_ts_start_syncaxi,
    output t0_seen_syncaxi,
    output [FRAME_NO_WIDTH-1:0] frame_number_syncaxi,
    output [TS_WIDTH-1:0] ts_shutter_open_syncaxi,
    output [TS_WIDTH-1:0] ts_shutter_close_syncaxi,
    output [TS_WIDTH-1:0] ts_trigger_syncaxi
  );

  logic [FRAME_NO_WIDTH-1:0] frame_number_syncts;
  logic [TS_WIDTH-1:0] ts_shutter_open_syncts;
  logic [TS_WIDTH-1:0] ts_shutter_close_syncts;
  logic [TS_WIDTH-1:0] ts_trigger_syncts;
  logic srst;
  logic t0;
  logic t0_seen;
  logic conf_ts_start_syncts;

  sync_bus #(
      .WIDTH(FRAME_NO_WIDTH),
      .STAGES_SIG(2),
      .STAGES_ACK(2)
  ) sync_frame_no (
      .in_clk(daq_clk),
      .out_clk(axi_clk),
      .bus_in(frame_number_syncts),
      .bus_out(frame_number_syncaxi)
  );

  sync_bus #(
      .WIDTH(TS_WIDTH),
      .STAGES_SIG(2),
      .STAGES_ACK(2)
  ) sync_ts_shutter_open (
      .in_clk(daq_clk),
      .out_clk(axi_clk),
      .bus_in(ts_shutter_open_syncts),
      .bus_out(ts_shutter_open_syncaxi)
  );

  sync_bus #(
      .WIDTH(TS_WIDTH),
      .STAGES_SIG(2),
      .STAGES_ACK(2)
  ) sync_ts_shutter_close (
      .in_clk(daq_clk),
      .out_clk(axi_clk),
      .bus_in(ts_shutter_close_syncts),
      .bus_out(ts_shutter_close_syncaxi)
  );

  sync_bus #(
      .WIDTH(TS_WIDTH),
      .STAGES_SIG(2),
      .STAGES_ACK(2)
  ) sync_ts_trigger (
      .in_clk(daq_clk),
      .out_clk(axi_clk),
      .bus_in(ts_trigger_syncts),
      .bus_out(ts_trigger_syncaxi)
  );

  sync_pulse #(
    .WIDTH(1),
    .STAGES_SIG(2),
    .STAGES_ACK(2)
  ) sync_ts_start (
    .in_clk(axi_clk),
    .out_clk(daq_clk),
    .pulse_in(conf_ts_start_syncaxi),
    .pulse_out(conf_ts_start_syncts)
  );

  sync_signal #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_pg_trig (
      .out_clk(axi_clk),
      .signal_in (t0_seen),
      .signal_out(t0_seen_syncaxi)
  );

  sync_reset #(
    .NEGATIVE(0),
    .STAGES(2)
  ) sync_ts_rst (
    .clk(daq_clk),
    .arst_in(arst),
    .rst_out(srst)
  );

  sync_reset #(
    .NEGATIVE(0),
    .STAGES(1)
  ) sync_t0 (
    .clk(daq_clk),
    .arst_in(t0_async),
    .rst_out(t0)
  );


  logic [TS_WIDTH-1:0] ts_counter;
  logic t0_prev = 1'b0;
  logic t0_go;
  logic tsrst;
  logic shutter_prev = 1'b0;
  logic trigger_seen;

  assign tsrst = srst || conf_ts_start_syncts;
  assign t0_go = ~t0_seen && (t0_prev && ~t0);

  always_ff @ (posedge daq_clk) begin
    shutter_prev <= shutter_syncdaq;
    t0_prev <= t0;
  end

  always_ff @ (posedge daq_clk) begin
    if (tsrst) begin
      t0_seen <= 1'b0;
    end
    else if (t0_go) begin
      t0_seen <= 1'b1;
    end
  end

  always_ff @(posedge daq_clk) begin
    if (tsrst || t0_go) begin
      ts_counter <= {TS_WIDTH{1'b0}};
    end
    else begin
      ts_counter <= ts_counter + 1;
    end
  end

  // count frames and store ts
  always_ff @ (posedge daq_clk) begin
    if (tsrst) begin
      frame_number_syncts <= {FRAME_NO_WIDTH{1'b0}};
      ts_shutter_open_syncts <= {TS_WIDTH{1'b0}};
      ts_shutter_close_syncts <= {TS_WIDTH{1'b0}};
      ts_trigger_syncts <= {TS_WIDTH{1'b0}};
      trigger_seen <= 1'b0;
    end
    else begin
      if (trigger_syncdaq && shutter_syncdaq && ~trigger_seen) begin
        //store trigger ts only when shutter is open and only the first one
        trigger_seen <= 1'b1;
        ts_trigger_syncts <= ts_counter;
      end
      if (shutter_syncdaq && ~shutter_prev) begin
        // shutter opens, new frame starts
        frame_number_syncts <= frame_number_syncts +1;
        ts_shutter_open_syncts <= ts_counter;
        ts_shutter_close_syncts <= {TS_WIDTH{1'b0}};
        ts_trigger_syncts <= {TS_WIDTH{1'b0}};
        trigger_seen <= 1'b0;
      end
      if (~shutter_syncdaq && shutter_prev) begin
        //shutter closes
        ts_shutter_close_syncts <= ts_counter;
      end
    end
  end

endmodule
