module H2M_sc_receiver (
    input  arst,
    output rx_clk_out,

    input  rx_start,
    output rx_done,
    output rx_enabled,
    output [31:0] data0_received,
    output [31:0] data1_received,
    output [31:0] data2_received,
    output [31:0] data3_received,
    output [15:0] address_received,
    output read_flag_received,
    output err_cmd_received,
    output err_addr_received,
    //output err_timeout,

    input  chip_SC_CLK_OUT,
    input  chip_SC_DATA_OUT
);

  logic rx_clk;
  logic rx_rst;
  logic sc_rx_sdata = 1'b1;
  logic rx_sync;
  logic [31:0] rx_shr = 32'hFFFFFFFF;
  logic rx_long_data;
  logic rx_read_flag;
  logic [15:0] rx_addr;
  logic [7:0]  rx_status;
  logic [31:0] rx_data0;
  logic [31:0] rx_data1;
  logic [31:0] rx_data2;
  logic [31:0] rx_data3;
  logic [2:0]  rx_data_ptr;
  logic [2:0]  rx_current_state = 3'b000;
  logic [2:0]  rx_next_state;
  logic [4:0]  rx_bitcounter;

  logic rx_en;
  logic rx_load_8;
  logic rx_load_16;
  logic rx_load_32;
  logic rx_store_addr;
  logic rx_store_rflag;
  logic rx_store_status;
  logic rx_store_data16;
  logic rx_store_ldata0;
  logic rx_store_ldata1;
  logic rx_store_ldata2;
  logic rx_store_ldata3;
  logic rx_finished;




  assign rx_clk = chip_SC_CLK_OUT;
  assign rx_clk_out = rx_clk;
  assign data0_received = rx_data0;
  assign data1_received = rx_data1;
  assign data2_received = rx_data2;
  assign data3_received = rx_data3;
  assign address_received = rx_addr;
  assign read_flag_received = rx_read_flag;
  assign err_cmd_received = rx_status[0];
  assign err_addr_received = rx_status[2];
  assign rx_done = rx_finished;
  assign rx_enabled = rx_en;

  sync_reset #(
    .NEGATIVE(0),
    .STAGES(2)
  ) sync_rx_rst (
    .clk(rx_clk),
    .arst_in(arst),
    .rst_out(rx_rst)
  );

  // TODO: add timeout
  always_ff @ (posedge rx_clk) begin
    if (rx_rst || rx_finished) begin
      rx_en <= 1'b0;
    end
    else if (rx_start) begin
      rx_en <= 1'b1;
    end
  end

  // rx input sync DFF
  always_ff @ (posedge rx_clk) begin
    if (rx_rst) sc_rx_sdata <= 1'b1;
    else sc_rx_sdata <= chip_SC_DATA_OUT;
  end

  // rx shift register
  always_ff @ (posedge rx_clk) begin
    if (rx_rst || !rx_en) begin
      rx_shr <= 32'hFFFFFFFF;
    end
    else begin
      if (rx_en) begin
        rx_shr[31:0] <= {rx_shr[30:0], sc_rx_sdata};
      end
    end
  end

  // detect sync pattern
  assign rx_sync = (rx_shr[23:1] == 23'b10101010000000000000000) ? 1'b1 : 1'b0;

  // rx state machine transition
  always_ff @ (posedge rx_clk) begin
    if (rx_rst || !rx_en) rx_current_state <= 3'b000;
    else rx_current_state <= rx_next_state;
  end

  // rx next state logic
  always_comb begin
    // default is to stay in current state
    rx_next_state = rx_current_state;
    case (rx_current_state)
      3'b000 : // idle
        if(rx_sync && rx_en)
          rx_next_state = 3'b001;
      3'b001 : // rx_addr
        if(rx_bitcount_zero)
          rx_next_state = 3'b010;
      3'b010 : // rx_status
        if(rx_bitcount_zero)
          if (rx_read_flag)
            if (rx_long_data) rx_next_state = 3'b100;
            else              rx_next_state = 3'b011;
          else rx_next_state = 3'b000;
      3'b011 : // rx_data16
        if(rx_bitcount_zero)
          rx_next_state = 3'b000;
      3'b100 : // rx_ldata0
        if(rx_bitcount_zero)
          rx_next_state = 3'b101;
      3'b101 : // rx_ldata1
        if(rx_bitcount_zero)
          rx_next_state = 3'b110;
      3'b110 : // rx_ldata2
        if(rx_bitcount_zero)
          rx_next_state = 3'b111;
      3'b111 : // rx_ldata3
        if(rx_bitcount_zero)
          rx_next_state = 3'b000;
      default : begin
        rx_next_state = 3'b000;
      end
    endcase
  end

  // rx output (control) signal logic
  always_comb begin
    // default values
    rx_load_8 = 1'b0;
    rx_load_16 = 1'b0;
    rx_load_32 = 1'b0;
    rx_store_addr = 1'b0;
    rx_store_rflag = 1'b0;
    rx_store_status = 1'b0;
    rx_store_data16 = 1'b0;
    rx_store_ldata0 = 1'b0;
    rx_store_ldata1 = 1'b0;
    rx_store_ldata2 = 1'b0;
    rx_store_ldata3 = 1'b0;
    rx_finished = 1'b0;

    case (rx_current_state)
      3'b000 : begin // idle
        if (rx_sync && rx_en) begin
          rx_store_rflag = 1'b1;
          rx_load_16 = 1'b1;
        end
      end
      3'b001 : begin // rx_addr
        if(rx_bitcount_zero) begin
          rx_store_addr = 1'b1;
          rx_load_8 = 1'b1;
        end
      end
      3'b010 : begin // rx_status
        if(rx_bitcount_zero) begin
          rx_store_status = 1'b1;
          if (rx_read_flag) begin
            if (rx_long_data) rx_load_32 = 1'b1;
            else              rx_load_16 = 1'b1;
          end
          else rx_finished = 1'b1;
        end
      end
      3'b011 : begin // rx_data16
        if(rx_bitcount_zero) begin
          rx_store_data16 = 1'b1;
          rx_finished = 1'b1;
        end
      end
      3'b100 : begin // rx_ldata3
        if(rx_bitcount_zero) begin
          rx_store_ldata3 = 1'b1;
          rx_load_32 = 1'b1;
        end
      end
      3'b101 : begin // rx_ldata2
        if(rx_bitcount_zero) begin
          rx_store_ldata2 = 1'b1;
          rx_load_32 = 1'b1;
        end
      end
      3'b110 : begin // rx_ldata1
        if(rx_bitcount_zero) begin
          rx_store_ldata1 = 1'b1;
          rx_load_32 = 1'b1;
        end
      end
      3'b111 : begin // rx_ldata0
        if(rx_bitcount_zero) begin
          rx_store_ldata0 = 1'b1;
          rx_finished = 1'b1;
        end
      end
      default: begin
      end
    endcase
  end

  // counting received bits
  assign rx_bitcount_zero = ~|rx_bitcounter;
  always_ff @ (posedge rx_clk) begin
    if (rx_rst || !rx_en)       rx_bitcounter <= 5'h00;
    else if (rx_load_8)         rx_bitcounter <= 5'h07;
    else if (rx_load_16)        rx_bitcounter <= 5'h0F;
    else if (rx_load_32)        rx_bitcounter <= 5'h1F;
    else if (!rx_bitcount_zero) rx_bitcounter <= rx_bitcounter -1;
  end

  // detect address of a long register
  assign rx_long_data = (rx_addr[15:12] == 4'hA) ?  1'b1 : 1'b0;

  // received data registers
  always_ff @ (posedge rx_clk) begin
    if (rx_rst) rx_read_flag <= 1'b0;
    else if (rx_store_rflag) rx_read_flag <= rx_shr[0];
  end

  always_ff @ (posedge rx_clk) begin
    if (rx_rst) rx_addr <= 1'b0;
    else if (rx_store_addr) rx_addr <= rx_shr[15:0];;
  end

  always_ff @ (posedge rx_clk) begin
    if (rx_rst) rx_status <= 1'b0;
    else if (rx_store_status) rx_status <= rx_shr[7:0];
  end

  always_ff @ (posedge rx_clk) begin
    if (rx_rst) rx_data0 <= 1'b0;
    else if (rx_store_ldata0) rx_data0 <= rx_shr[31:0];
  end

  always_ff @ (posedge rx_clk) begin
    if (rx_rst) rx_data1 <= 1'b0;
    else if (rx_store_ldata1) rx_data1 <= rx_shr[31:0];
  end

  always_ff @ (posedge rx_clk) begin
    if (rx_rst) rx_data2 <= 1'b0;
    else if (rx_store_ldata2) rx_data2 <= rx_shr[31:0];
  end

  always_ff @ (posedge rx_clk) begin
    if (rx_rst) rx_data3 <= 1'b0;
    else if (rx_store_data16) rx_data3 <= {16'h0000, rx_shr[15:0]};
    else if (rx_store_ldata3) rx_data3 <= rx_shr[31:0];
  end

//  ila_sc_receiver ila_sc_receiver_inst (
//    .clk(rx_clk), // input wire clk
//    .probe0(rx_shr), // input wire [31:0]  probe0
//    .probe1(rx_addr), // input wire [15:0]  probe1
//    .probe2(rx_status), // input wire [7:0]  probe2
//    .probe3(rx_data0), // input wire [31:0]  probe3
//    .probe4(rx_data1), // input wire [31:0]  probe4
//    .probe5(rx_data2), // input wire [31:0]  probe5
//    .probe6(rx_data3), // input wire [31:0]  probe6
//    .probe7(rx_data_ptr), // input wire [2:0]  probe7
//    .probe8(rx_current_state), // input wire [2:0]  probe8
//    .probe9(rx_next_state), // input wire [2:0]  probe9
//    .probe10(rx_bitcounter), // input wire [4:0]  probe10
//    .probe11(rx_rst), // input wire [0:0]  probe11
//    .probe12(sc_rx_sdata), // input wire [0:0]  probe12
//    .probe13(rx_sync), // input wire [0:0]  probe13
//    .probe14(rx_long_data), // input wire [0:0]  probe14
//    .probe15(rx_read_flag), // input wire [0:0]  probe15
//    .probe16(rx_en), // input wire [0:0]  probe16
//    .probe17(rx_load_8), // input wire [0:0]  probe17
//    .probe18(rx_load_16), // input wire [0:0]  probe18
//    .probe19(rx_load_32), // input wire [0:0]  probe19
//    .probe20(rx_store_addr), // input wire [0:0]  probe20
//    .probe21(rx_store_rflag), // input wire [0:0]  probe21
//    .probe22(rx_store_status), // input wire [0:0]  probe22
//    .probe23(rx_store_data16), // input wire [0:0]  probe23
//    .probe24(rx_store_ldata0), // input wire [0:0]  probe24
//    .probe25(rx_store_ldata1), // input wire [0:0]  probe25
//    .probe26(rx_store_ldata2), // input wire [0:0]  probe26
//    .probe27(rx_store_ldata3), // input wire [0:0]  probe27
//    .probe28(rx_finished), // input wire [0:0]  probe28
//    .probe29(rx_start) // input wire [0:0]  probe29
//  );

endmodule
