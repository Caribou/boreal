`timescale 1ns / 1ps

//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat, Jan Pospisil
//
// Module Name: sync_bus
//
// Description:
//   Synchronizes bus signal in CDC, based on bus change (no strobe signal).
//   Only for spare changes. For a too quick burst of changes, not all intermediate
//   values will be propagated. The last change will always be propagated.
//
//
// Revision:
// 12.10.2017  1  J. Pospisil <j.pospisil@cern.ch>  first version
// 03.04.2018  2  T. Vanat    <tomas.vanat@cern.ch> rewritten to systemverilog,
//                                                  changed port names
// 30.03.2023  3  T. Vanat    <tomas.vanat@desy.de> major changes, simplification
//                                                  based on syncing T-FFs,
//                                                  no dependencies
//
//////////////////////////////////////////////////////////////////////////////////

module sync_bus #(
    int WIDTH  = 1,
    int STAGES_SIG = 1,
    int STAGES_ACK = 1
  )(
    input in_clk,
    input out_clk,
    input  [WIDTH-1:0] bus_in,
    output [WIDTH-1:0] bus_out
);

  logic [WIDTH-1:0] bus_in_stored = {(WIDTH){1'b0}};
  (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic [WIDTH-1:0] bus_out_reg   = {(WIDTH){1'b0}};

  (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic T_ff_sync [STAGES_SIG : 0] = '{default:{1'b0}};
  (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic T_ff_ack  [STAGES_ACK : 0] = '{default:{1'b0}};

  logic T_ff_in           = 1'b0;
  logic T_ff_out          = 1'b0;
  logic input_capture;


  assign bus_out  = bus_out_reg;

  assign input_capture = ~(T_ff_in ^ T_ff_ack[STAGES_ACK]);

  // capture input if transition is not active and initiate transition on input change
  always_ff @(posedge in_clk) begin
    if (input_capture) begin
      bus_in_stored <= bus_in;
      if (|(bus_in_stored ^ bus_in)) begin
        T_ff_in <= ~T_ff_in;
      end
    end
  end

  // sync bus change in->out
  always_ff @(posedge out_clk) begin
    T_ff_sync[0] <= T_ff_in;
    for (int j=1; j <= STAGES_SIG; j=j+1) begin
      T_ff_sync[j] <= T_ff_sync[j-1];
    end
    T_ff_out <= T_ff_sync[STAGES_SIG];
  end

  // write output bus
  always_ff @(posedge out_clk) begin
    if(T_ff_out ^ T_ff_sync[STAGES_SIG])
      bus_out_reg <= bus_in_stored;
  end

  // sync ack out->in
  always_ff @(posedge in_clk) begin
    T_ff_ack[0] <= T_ff_out;
    for (int j=1; j <= STAGES_ACK; j=j+1) begin
      T_ff_ack[j] <= T_ff_ack[j-1];
    end
  end

endmodule
