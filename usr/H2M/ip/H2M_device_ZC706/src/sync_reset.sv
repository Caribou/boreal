`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
//
// Module Name: sync_reset
//
// Description:
//   Synchronizes de-assertion of a reset signal to a clock domain
//   RESET is asserted immediately (asynchronously), deassertion is synced
//
//////////////////////////////////////////////////////////////////////////////////

// reset sync
module sync_reset #(
    logic NEGATIVE = 0,
    int   STAGES   = 2
  )(
    input  clk,
    input  arst_in,  // i, async, could be set to active low
    output rst_out   // o, sync to clk, always active high
);

  (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic [STAGES:0] reset_sync;

  logic arst;
  logic rst_out_reg;

  assign arst = arst_in ^ NEGATIVE;
  assign rst_out = rst_out_reg;

  always_ff @(posedge clk, posedge arst) begin
    if (arst) begin
      reset_sync[0] <= 1'b1;
      rst_out_reg   <= 1'b1;
    end
    else begin
      reset_sync[0] <= 1'b0;
      rst_out_reg   <= |reset_sync;
    end
  end

  genvar i;
  generate
  for (i=1; i <= STAGES; i=i+1) begin
    always_ff @(posedge clk, posedge arst) begin
      if (arst)
        reset_sync[i] <= 1'b1;
      else
        reset_sync[i] <= reset_sync[i-1];
    end
  end
  endgenerate

endmodule
