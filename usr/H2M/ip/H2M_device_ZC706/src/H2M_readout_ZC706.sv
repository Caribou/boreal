`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Younes Otarid
//
// Create Date: 07/20/2023 04:07:16 PM
// Design Name: h2m_fw
// Module Name: h2m_readout
// Project Name: H2M
// Target Devices:
// Tool Versions: Vivado 2019.2
// Description: H2M Readout block retrieving hit counter data and buffering them into a FIFO
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////

module H2M_readout #(
    parameter [15:0] ADDR_GLOBAL_CTRL = 16'h0001,
    parameter [15:0] ADDR_COL_0 = 16'hA000
) (
    //global signals
    input logic axi_clk_i,
    input logic reset_i,

    //readout control interface
    //    input set_global_conf_i,
    //    input clear_global_conf_i,
    input  logic acq_start_i,
    input  logic acq_stop_i,
    input  logic start_readout_i,
    output logic idle_o,
    output logic err_o,
    output logic busy,

    //slow-control control interface
    output logic [15:0] addr_write_o,
    output logic [31:0] data_write_o,
    output logic addr_write_wstrobe_o,
    output logic [15:0] addr_read_o,
    output logic addr_read_wstrobe_o,
    //
    input logic [15:0] global_ctrl_i,

    input logic [15:0] addr_received_i,
    input logic [31:0] data_received_i,
    input logic read_flag_received_i,
    output logic received_data_read_strobe_o,
    //
    input logic sc_stat_idle_i,
    input logic sc_err_i,
    // timestamps input
    input logic t0_seen,
    input logic [31:0] frame_number,
    input logic [47:0] ts_shutter_open,
    input logic [47:0] ts_shutter_close,
    input logic [47:0] ts_trigger,
    //FIFO interface
    output logic [31:0] fifo_data_out,
    output logic fifo_data_valid,
    input logic fifo_read,
    output logic [31:0] fifo_info
);

  logic fifo_almost_full;
  logic fifo_full;
  logic fifo_wr_en;
  logic [31:0] fifo_din;

  logic acq_err_o;
  logic fifo_empty;
  logic [31:0] fifo_dout;

  logic [7:0] col_cnt;
  logic [3:0] word_cnt;
  logic [7:0] head_cnt;
  logic [30:0] fifo_word_cnt;

  assign fifo_data_valid = ~fifo_empty;
  assign fifo_rd_en = fifo_read;
  assign fifo_data_out = fifo_dout;

  assign fifo_info[30:0] = fifo_word_cnt;
  //assign fifo_info[30:16] = 'b0;;
  assign fifo_info[31] = fifo_prog_empty;

  assign err_o = acq_err_o;
  assign busy = fifo_almost_full;

  //localparam [15:0] ADDR_COL_0 = 16'hA000; //address of first column in pixel matrix
  localparam [7:0] N_COL = 64;  //number of columns in pixel matrix
  localparam [3:0] N_WORDS = 4;  //nuber of words per readout
  localparam [7:0] N_HEAD = 6;  // number of header 32bit words

  logic [N_HEAD-1:0][31:0] header;
  logic [8:0] frame_length = 9'd256;

  //FSM states
  typedef enum logic [3:0] {
    IDLE,
    WAIT_RESPONSE,
    GET_DATA,
    WRITE_TIMESTAMP,
    BUSY
  } readout_state_type;
  //FSM state register
  readout_state_type readout_state, readout_state_next;

  logic sc_idle_no_strobe;
  assign sc_idle_no_strobe = sc_stat_idle_i & ~addr_write_wstrobe_o & ~addr_read_wstrobe_o;

  logic readout;

  assign idle_o = (readout_state == IDLE);

  //state machine transition process and reset process
  always_ff @(posedge axi_clk_i) begin
    if (reset_i) readout_state <= IDLE;
    else readout_state <= readout_state_next;
  end

  //state
  always_comb begin
    //always stay in the same state if there are no changes
    readout_state_next = readout_state;
    case (readout_state)
      IDLE: begin
        if (acq_start_i == 1 || acq_stop_i) readout_state_next = WAIT_RESPONSE;
        if (start_readout_i)
          if (~fifo_almost_full) begin
            readout_state_next = WRITE_TIMESTAMP;
            //readout_state_next = WAIT_RESPONSE;
          end else readout_state_next = BUSY;
      end
      BUSY: begin
        if (~fifo_almost_full) readout_state_next = WRITE_TIMESTAMP;
      end
      WRITE_TIMESTAMP: begin
        if (head_cnt == N_HEAD) readout_state_next = WAIT_RESPONSE;
      end
      WAIT_RESPONSE: begin
        if (sc_idle_no_strobe) begin
          if (sc_err_i) readout_state_next = IDLE;
          else begin
            if (readout) readout_state_next = GET_DATA;
            else readout_state_next = IDLE;
          end
        end
      end
      GET_DATA: begin
        if (col_cnt <= N_COL) readout_state_next = WAIT_RESPONSE;  // got the N_WORDS, move to next
        if (word_cnt == N_WORDS && col_cnt == N_COL) readout_state_next = IDLE;
      end
    endcase

  end

  //
  logic ts0, rd0, wr0, wrF, err;

  // ctrl
  always_comb begin
    ts0 = 1'b0;
    rd0 = 1'b0;
    wr0 = 1'b0;
    wrF = 1'b0;
    err = 1'b0;
    case (readout_state)
      IDLE: begin
        if (start_readout_i) begin
          ts0 = 1'b1;  // always initialize the header and the counters even if the fifo is full
        end else if (acq_start_i || acq_stop_i) wr0 = 1'b1;
      end
      BUSY: begin
        if (~fifo_almost_full) ts0 = 1'b1;  // when the fifo is ready, write the header
      end
      WRITE_TIMESTAMP: begin
        if (head_cnt == N_HEAD) rd0 = 1'b1;  // at the end of the timestamp issue the first readout
        else ts0 = 1'b1;
      end
      WAIT_RESPONSE: begin
        if (sc_idle_no_strobe) begin
          if (sc_err_i) err = 1'b1;
        end
      end
      GET_DATA: begin
        if (word_cnt < N_WORDS) begin
          wrF = 1'b1;
        end else if (col_cnt < N_COL) rd0 = 1'b1;

      end
    endcase
  end

  always_ff @(posedge axi_clk_i) begin
    if (reset_i) begin
      fifo_word_cnt <= 1'b0;
    end else begin
      if (fifo_wr_en && ~fifo_rd_en) fifo_word_cnt <= fifo_word_cnt + 1;
      if (~fifo_wr_en && fifo_rd_en && ~fifo_empty) fifo_word_cnt <= fifo_word_cnt - 1;
    end
  end

  always_ff @(posedge axi_clk_i) begin
    if (reset_i) begin
      col_cnt              <= 8'd0;
      word_cnt             <= 4'd0;
      head_cnt             <= 8'd0;
      addr_write_wstrobe_o <= 1'b0;
      addr_read_wstrobe_o  <= 1'b0;
      fifo_wr_en           <= 1'b0;
      fifo_din             <= 32'd0;
      addr_write_o         <= 16'd0;
      data_write_o         <= 32'd0;
      addr_read_o          <= 16'd0;
      readout              <= 1'b0;
      acq_err_o            <= 1'b0;
      header               <= '{default: 'd0};
    end else begin
      addr_write_wstrobe_o <= 1'b0;
      addr_read_wstrobe_o  <= 1'b0;
      fifo_wr_en           <= 1'b0;
      if (wr0) begin
        if (acq_start_i == 1) data_write_o <= {16'd0, global_ctrl_i[15:2], 2'b01};  // set acq_start
        else if (acq_stop_i == 1)
          data_write_o <= {16'd0, global_ctrl_i[15:2], 2'b00};  // reset acq_start
        addr_write_o         <= ADDR_GLOBAL_CTRL;
        addr_write_wstrobe_o <= 1'b1;
        readout              <= 1'b0;
        acq_err_o            <= 1'b0;
      end
      if (rd0 || wr0) begin
        acq_err_o <= 1'b0;
      end
      if (ts0) begin
        if (start_readout_i) begin
          col_cnt         <= 8'd0;
          head_cnt        <= 8'd0;
          header[0][31:0] <= {ts_trigger[47:32], t0_seen, 6'b0, frame_length[8:0]};
          header[1][31:0] <= {ts_trigger[31:0]};
          header[2][31:0] <= {ts_shutter_open[31:0]};
          header[3][31:0] <= {ts_shutter_close[47:32], ts_shutter_open[47:32]};
          header[4][31:0] <= {ts_shutter_close[31:0]};
          header[5][31:0] <= {frame_number[31:0]};
        end else begin
          head_cnt   <= head_cnt + 1;
          fifo_wr_en <= 1'b1;
          fifo_din   <= header[head_cnt][31:0];
        end
      end
      if (rd0) begin
        word_cnt            <= 4'd0;
        col_cnt             <= col_cnt + 1;
        addr_read_o         <= ADDR_COL_0 + col_cnt;
        addr_read_wstrobe_o <= 1'b1;
        readout             <= 1'b1;
      end
      if (wrF) begin
        // from previous addr_read_wstrobe_o the data is available. Store.
        word_cnt   <= word_cnt + 1;
        fifo_wr_en <= 1'b1;
        fifo_din   <= data_received_i;
        if (word_cnt < N_WORDS - 1)  // word_cnt is updated only on the next cycle
          addr_read_wstrobe_o <= 1'b1; // get next data. Last does not need to assert the read_wstrobe or a new readout starts
        //if (word_cnt == N_WORDS-1 && col_cnt == N_COL)
        //    frame_cnt <= frame_cnt+1;
      end
      if (err) begin
        acq_err_o <= 1'b1;
      end
    end
  end

  // FIXME: suggested data format:
  // 32b: {ts_trigger[47:32] , t0_seen[0:0], reserved[5:0], frame_length[8:0]}
  // 32b: {ts_trigger[31:0]}
  // 32b: {ts_shutter_open[31:0]}
  // 32b: {ts_shutter_close[47:32], ts_shutter_open[47:32]}
  // 32b: {ts_shutter_close[31:0]}
  // 32b: {frame_number[31:0]}
  // 64*4*32b{chip_data}

  //FIFO should assert almost_full flag when the remaining space is less than what is needed for a full frame

  //Readout FIFO
  H2M_readout_fifo_ZC706 H2M_readout_fifo_ZC706_inst (
      .clk(axi_clk_i),
      //.srst(reset_i), // not in builtin fifo

      .rst(reset_i),

      //write
      //.almost_full(),
      .full (fifo_full),
      .wr_en(fifo_wr_en),
      .din  (fifo_din),

      //read
      //.almost_empty(), // not in builtin fifo
      .empty(fifo_empty),
      .rd_en(fifo_rd_en),
      .dout (fifo_dout),

      // set to fifo length-full frame with header
      .prog_full (fifo_almost_full),
      .prog_empty(fifo_prog_empty)
      // get the data length
      //.data_count(fifo_data_cnt)
  );

  // ila_readout ila_readout_inst (
  //     .clk(axi_clk_i),  // input wire clk
  //     .probe0(acq_start_i),  // input wire [0:0]  probe0
  //     .probe1(acq_stop_i),  // input wire [0:0]  probe1
  //     .probe2(start_readout_i),  // input wire [0:0]  probe2
  //     .probe3(idle_o),  // input wire [0:0]  probe3
  //     .probe4(err_o),  // input wire [0:0]  probe4
  //     .probe5(busy),  // input wire [0:0]  probe5

  //     .probe6(addr_write_o),  // input wire [15:0]  probe6
  //     .probe7(data_write_o),  // input wire [31:0]  probe7
  //     .probe8(addr_write_wstrobe_o),  // input wire [0:0]  probe8
  //     .probe9(addr_read_o),  // input wire [15:0]  probe9
  //     .probe10(addr_read_wstrobe_o),  // input wire [0:0]  probe10

  //     .probe11(global_ctrl_i),  // input wire [15:0]  probe11

  //     .probe12(addr_received_i),  // input wire [15:0]  probe12
  //     .probe13(data_received_i),  // input wire [31:0]  probe13
  //     .probe14(read_flag_received_i),  // input wire [0:0]  probe14
  //     .probe15(received_data_read_strobe_o),  // input wire [0:0]  probe15

  //     .probe16(sc_stat_idle_i),  // input wire [0:0]  probe16
  //     .probe17(sc_err_i),  // input wire [0:0]  probe17
  //     .probe18(t0_seen),  // input wire [0:0]  probe18

  //     .probe19(frame_number),  // input wire [31:0]  probe19
  //     .probe20(ts_shutter_open),  // input wire [47:0]  probe20
  //     .probe21(ts_shutter_close),  // input wire [47:0]  probe21
  //     .probe22(ts_trigger),  // input wire [47:0]  probe22

  //     .probe23(fifo_data_out),  // input wire [31:0]  probe23
  //     .probe24(fifo_data_valid),  // input wire [0:0]  probe24
  //     .probe25(fifo_read),  // input wire [0:0]  probe25
  //     .probe26(fifo_info)  // input wire [31:0]  probe26
  // );
endmodule
