`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
//
// Module Name: sync_pulse
//
// Description:
//   generates a pulse for one out_clk cycle each time the signal_in is 1
//   pulses in the vector are not guaranted to propagate in sync
//
//////////////////////////////////////////////////////////////////////////////////

module sync_pulse #(
    int WIDTH  = 1,
    int STAGES_SIG = 1,
    int STAGES_ACK = 1
 )(
    input in_clk,
    input out_clk,
    input  [WIDTH-1 :0] pulse_in,  // i, sync to in_clk
    output [WIDTH-1 :0] pulse_out // o, sync to out_clk
);

  (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic [WIDTH-1 : 0] T_ff_sync [STAGES_SIG : 0] = '{default:{(WIDTH){1'b0}}};
  (* ASYNC_REG = "TRUE", keep = "true", shreg_extract = "no" *) logic [WIDTH-1 : 0] T_ff_ack  [STAGES_ACK : 0] = '{default:{(WIDTH){1'b0}}};

  logic [WIDTH-1 :0] T_ff_in           = 1'b0;
  logic [WIDTH-1 :0] T_ff_out          = 1'b0;
  logic [WIDTH-1 :0] input_capture;

  assign input_capture = ~(T_ff_in ^ T_ff_ack[STAGES_ACK]);

  // capture input pulse
  always_ff @(posedge in_clk)
    for (int i=0; i < WIDTH; i=i+1)
      if (input_capture[i] & pulse_in[i])
        T_ff_in[i] <= ~T_ff_in[i];

  // sync signal change in->out
  always_ff @(posedge out_clk) begin
    T_ff_sync[0] <= T_ff_in;
    for (int j=1; j <= STAGES_SIG; j=j+1) begin
      T_ff_sync[j] <= T_ff_sync[j-1];
    end
    T_ff_out <= T_ff_sync[STAGES_SIG];
  end

  // generate output pulse
  assign pulse_out = T_ff_out ^ T_ff_sync[STAGES_SIG];

  // sync ack out->in
  always_ff @(posedge in_clk) begin
    T_ff_ack[0] <= T_ff_sync[STAGES_SIG];
    for (int j=1; j <= STAGES_ACK; j=j+1) begin
      T_ff_ack[j] <= T_ff_ack[j-1];
    end
  end

endmodule
