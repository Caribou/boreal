module H2M_sc_interface (
    input  tx_clk,
    input  axi_clk,
    input  rst,

    input  [15:0] addr_write,
    input  [31:0] data_write,
    input  addr_write_wstrobe,
    input  [15:0] addr_read,
    input  addr_read_wstrobe,

    output [15:0] addr_received,
    output [31:0] data_received,
    output read_flag_received,
    input  received_data_read_strobe,

    output stat_sc_idle,
    output err_cmd,
    output err_addr,
    output err_timeout,
    output testpulse_wait,
    //output err_unexpected_addr,

    output chip_SC_CLK_IN,
    output chip_SC_DATA_IN,
    input  chip_SC_CLK_OUT,
    input  chip_SC_DATA_OUT
);

  logic waiting_for_data_i;
  logic sc_timeout = 1'b1;
  logic testpulse;
  logic testpulse_wait_i;
  logic rx_buff_rst;

  assign testpulse_wait = testpulse_wait_i;
  assign err_timeout = sc_timeout;

  ///////////////////////
  // transmitter setup //
  ///////////////////////

  logic [2:0] wtx_state = 3'b000;
  logic [2:0] wtx_state_next;
  logic [15:0] addr_stored = 16'h00FF;
  logic addrw_long;
  logic addrr_long;
  logic addrw_prev_equal;
  logic addrr_prev_equal;
  logic [127:0] data_write_full = 128'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
  logic tx_rl;
  logic tx_rd;
  logic tx_ws;
  logic tx_w0;
  logic tx_w1;
  logic tx_w2;
  logic tx_w3;
  logic tx_cmd_write_short_axi = 1'b0;
  logic tx_cmd_write_long_axi = 1'b0;
  logic tx_cmd_read_axi = 1'b0;
  logic set_wait_flag = 1'b0;
  logic tx_cmd_write_short;
  logic tx_cmd_write_long;
  logic tx_cmd_read;
  logic [15:0] tx_addr;
  logic [127:0] tx_data;
  logic comm_start;
  logic tx_done;

  assign addrw_long = (addr_write[15:12] == 4'hA) ? 1'b1 : 1'b0;
  assign addrr_long = (addr_read[15:12] == 4'hA) ? 1'b1 : 1'b0;
  assign addrw_prev_equal = (addr_write[7:0] == addr_stored[7:0]) ? 1'b1 : 1'b0;
  assign addrr_prev_equal = (addr_read[7:0] == addr_stored[7:0]) ? 1'b1 : 1'b0;

  always_ff @ (posedge axi_clk) begin
    if (rst) wtx_state <= 3'b00;
    else     wtx_state <= wtx_state_next;
  end

  // write state
  always_comb begin
    wtx_state_next = wtx_state;
    case (wtx_state)
      3'b000: begin // idle
        if (addr_read_wstrobe)
          if (addrr_long)           wtx_state_next = 3'b101; // long read
          else                      wtx_state_next = 3'b000; // short read
        else if (addr_write_wstrobe)
          if (addrw_long)           wtx_state_next = 3'b001; // long write
          else                      wtx_state_next = 3'b000; // short write
      end
      3'b001: begin // wr_long0
        if (addr_read_wstrobe)
          if (addrr_long)           wtx_state_next = 3'b101; // long read
          else                      wtx_state_next = 3'b000; // short read
        else if (addr_write_wstrobe)
          if (addrw_long)
            if (addrw_prev_equal)   wtx_state_next = 3'b010; // long write, same address
            else                    wtx_state_next = 3'b001; // long write, different address
          else                      wtx_state_next = 3'b000; // short write
      end
      3'b010: begin // wr_long1
        if (addr_read_wstrobe)
          if (addrr_long)           wtx_state_next = 3'b101; // long read
          else                      wtx_state_next = 3'b000; // short read
        else if (addr_write_wstrobe)
          if (addrw_long)
            if (addrw_prev_equal)   wtx_state_next = 3'b011; // long write, same address
            else                    wtx_state_next = 3'b001; // long write, different address
          else                      wtx_state_next = 3'b000; // short write
      end
      3'b011: begin // wr_long2
        if (addr_read_wstrobe)
          if (addrr_long)           wtx_state_next = 3'b101; // long read
          else                      wtx_state_next = 3'b000; // short read
        else if (addr_write_wstrobe)
          if (addrw_long)
            if (addrw_prev_equal)   wtx_state_next = 3'b000; // long write, same address
            else                    wtx_state_next = 3'b001; // long write, different address
          else                      wtx_state_next = 3'b000; // short write
      end
      3'b101: begin // rd_long0
          if (addr_read_wstrobe)
            if (addrr_long)
              if (addrr_prev_equal) wtx_state_next = 3'b110; // long read, same address
              else                  wtx_state_next = 3'b101; // long read, different address
            else                    wtx_state_next = 3'b000; // short read
        else if (addr_write_wstrobe)
          if (addrw_long)           wtx_state_next = 3'b001; // long write
          else                      wtx_state_next = 3'b000; // short write
      end
      3'b110: begin // rd_long1
          if (addr_read_wstrobe)
            if (addrr_long)
              if (addrr_prev_equal) wtx_state_next = 3'b111; // long read, same address
              else                  wtx_state_next = 3'b101; // long read, different address
            else                    wtx_state_next = 3'b000; // short read
        else if (addr_write_wstrobe)
          if (addrw_long)           wtx_state_next = 3'b001; // long write
          else                      wtx_state_next = 3'b000; // short write
      end
      3'b111: begin // rd_long2
          if (addr_read_wstrobe)
            if (addrr_long)
              if (addrr_prev_equal) wtx_state_next = 3'b000; // long read, same address
              else                  wtx_state_next = 3'b101; // long read, different address
            else                    wtx_state_next = 3'b000; // short read
        else if (addr_write_wstrobe)
          if (addrw_long)           wtx_state_next = 3'b001; // long write
          else                      wtx_state_next = 3'b000; // short write
      end

      default: wtx_state_next = 3'b000;
    endcase
  end

  // write control signals
  always_comb begin
    tx_ws = 1'b0;
    tx_w0 = 1'b0;
    tx_w1 = 1'b0;
    tx_w2 = 1'b0;
    tx_w3 = 1'b0;
    tx_rd = 1'b0;
    tx_rl = 1'b0;
    case (wtx_state)
      3'b000: begin // idle / initiate transaction
        if (addr_read_wstrobe)      tx_rd = 1'b1; // read
        else if (addr_write_wstrobe)
          if (addrw_long)           tx_w3 = 1'b1; // long write
          else                      tx_ws = 1'b1; // short write
      end
      3'b001: begin // wr_long0
        if (addr_read_wstrobe)      tx_rd = 1'b1; // read
        else if (addr_write_wstrobe)
          if (addrw_long)
            if (addrw_prev_equal)   tx_w2 = 1'b1; // long write, same address
            else                    tx_w3 = 1'b1; // long write, different address
          else                      tx_ws = 1'b1; // short write
      end
      3'b010: begin // wr_long1
        if (addr_read_wstrobe)      tx_rd = 1'b1; // read
        else if (addr_write_wstrobe)
          if (addrw_long)
            if (addrw_prev_equal)   tx_w1 = 1'b1; // long write, same address
            else                    tx_w3 = 1'b1; // long write, different address
          else                      tx_ws = 1'b1; // short write
      end
      3'b011: begin // wr_long2
        if (addr_read_wstrobe)      tx_rd = 1'b1; // read
        else if (addr_write_wstrobe)
          if (addrw_long)
            if (addrw_prev_equal)   tx_w0 = 1'b1; // long write, same address
            else                    tx_w3 = 1'b1; // long write, different address
          else                      tx_ws = 1'b1; // short write
      end
      3'b101: begin // rd_long0
        if (addr_read_wstrobe)
          if (addrr_long)
            if (addrr_prev_equal)   tx_rl = 1'b1; // long read, same address
            else                    tx_rd = 1'b1; // lond read, different address
          else                      tx_rd = 1'b1; // short read
        else if (addr_write_wstrobe)
          if (addrw_long)           tx_w3 = 1'b1; // long write
          else                      tx_ws = 1'b1; // short write
      end
      3'b110: begin // rd_long1
        if (addr_read_wstrobe)
          if (addrr_long)
            if (addrr_prev_equal)   tx_rl = 1'b1; // long read, same address
            else                    tx_rd = 1'b1; // lond read, different address
          else                      tx_rd = 1'b1; // short read
        else if (addr_write_wstrobe)
          if (addrw_long)           tx_w3 = 1'b1; // long write
          else                      tx_ws = 1'b1; // short write
      end
      3'b111: begin // rd_long2
        if (addr_read_wstrobe)
          if (addrr_long)
            if (addrr_prev_equal)   tx_rl = 1'b1; // long read, same address
            else                    tx_rd = 1'b1; // lond read, different address
          else                      tx_rd = 1'b1; // short read
        else if (addr_write_wstrobe)
          if (addrw_long)           tx_w3 = 1'b1; // long write
          else                      tx_ws = 1'b1; // short write
      end

    endcase
  end

  always_ff @ (posedge axi_clk) begin
    tx_cmd_write_short_axi  <= 1'b0;
    tx_cmd_write_long_axi   <= 1'b0;
    tx_cmd_read_axi         <= 1'b0;
    set_wait_flag           <= 1'b0;
    rx_buff_rst             <= 1'b0;
    if (rst) begin
      data_write_full[127:0] <= 128'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
      addr_stored <= 16'h00FF;
    end
    else begin
      if (tx_w3) begin
        data_write_full[127:96] <= data_write[31:0];
        addr_stored             <= addr_write;
        rx_buff_rst             <= 1'b1;
      end
      if (tx_w2) begin
        data_write_full[95:64]  <= data_write[31:0];
        rx_buff_rst             <= 1'b1;
      end
      if (tx_w1) begin
        data_write_full[63:32]  <= data_write[31:0];
        rx_buff_rst             <= 1'b1;
      end
      if (tx_w0) begin
        data_write_full[31:0]   <= data_write[31:0];
        tx_cmd_write_long_axi   <= 1'b1;
        set_wait_flag           <= 1'b1;
        rx_buff_rst             <= 1'b1;
      end
      if (tx_ws) begin
        data_write_full[15:0]   <= data_write[15:0];
        data_write_full[127:16] <= 112'hFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
        addr_stored             <= addr_write;
        tx_cmd_write_short_axi  <= 1'b1;
        set_wait_flag           <= 1'b1;
        rx_buff_rst             <= 1'b1;
      end
      if (tx_rd) begin
        addr_stored      <= addr_read;
        tx_cmd_read_axi  <= 1'b1;
        set_wait_flag    <= 1'b1;
        rx_buff_rst      <= 1'b1;
      end
    end
  end

  assign comm_start = tx_cmd_write_long_axi || tx_cmd_write_short_axi || tx_cmd_read_axi;

  sync_bus_with_strobe #(
    .WIDTH(147),
    .STAGES_SIG(1),
    .STAGES_ACK(1)
  ) sync_write_from_axi (
    .in_clk(axi_clk),
    .out_clk(tx_clk),
    .in_strobe(comm_start),
    .out_strobe(tx_start),
    .bus_in( {addr_stored, data_write_full, tx_cmd_write_long_axi, tx_cmd_write_short_axi, tx_cmd_read_axi}),
    .bus_out({tx_addr,     tx_data,         tx_cmd_write_long,     tx_cmd_write_short,     tx_cmd_read})
  );

  H2M_sc_transmitter H2M_sc_transmitter_inst (
    .tx_clk(tx_clk),
    .arst(rst),
    .tx_address(tx_addr),
    .tx_data(tx_data),
    .tx_cmd_write_short(tx_cmd_write_short && tx_start),
    .tx_cmd_write_long(tx_cmd_write_long && tx_start),
    .tx_cmd_read(tx_cmd_read && tx_start),
    .tx_done(tx_done),
    .tx_busy(),
    .chip_SC_CLK_IN(chip_SC_CLK_IN),
    .chip_SC_DATA_IN(chip_SC_DATA_IN)
  );

  //////////////////////////////
  // received data processing //
  //////////////////////////////

  logic rx_clk;
  logic rx_done;
  logic rx_done_axisync;
  logic rx_start_syncrx;
  logic long_rdata_shifted;
  logic [1:0] received_data_select = 2'b00;
  logic [31:0] data_received_i;

  logic [31:0] data0_received_axisync;
  logic [31:0] data1_received_axisync;
  logic [31:0] data2_received_axisync;
  logic [31:0] data3_received_axisync;
  logic [15:0] address_received_axisync;
  logic        read_flag_received_axisync;
  logic        long_data_received_axisync;
  logic        err_cmd_received_axisync;
  logic        err_addr_received_axisync;

  logic [31:0] rx_data0_received;
  logic [31:0] rx_data1_received;
  logic [31:0] rx_data2_received;
  logic [31:0] rx_data3_received;
  logic [15:0] rx_address_received;
  logic        rx_read_flag_received;
  logic        rx_err_cmd_received;
  logic        rx_err_addr_received;

  assign data_received = data_received_i;

  always_ff @ (posedge axi_clk) begin
    if (rst || rx_buff_rst) begin
      data0_received_axisync <= 32'b0;
      data1_received_axisync <= 32'b0;
      data2_received_axisync <= 32'b0;
      data3_received_axisync <= 32'b0;
      address_received_axisync <= 16'b0;
      read_flag_received_axisync <= 1'b0;
      err_cmd_received_axisync <= 1'b0;
      err_addr_received_axisync <= 1'b0;
    end
    else if (rx_done_axisync) begin
      data0_received_axisync <= rx_data0_received;
      data1_received_axisync <= rx_data1_received;
      data2_received_axisync <= rx_data2_received;
      data3_received_axisync <= rx_data3_received;
      address_received_axisync <= rx_address_received;
      read_flag_received_axisync <= rx_read_flag_received;
      err_cmd_received_axisync <= rx_err_cmd_received;
      err_addr_received_axisync <= rx_err_addr_received;
    end
  end

  // cout how many times was the received data read
  always_ff @ (posedge axi_clk) begin
    if (rst || waiting_for_data_i || rx_done_axisync) begin
      received_data_select <= 2'b00;
      long_rdata_shifted <= 1'b0;
    end
    else if (received_data_read_strobe && addrr_long) begin
      received_data_select <= received_data_select +1;
      long_rdata_shifted <= 1'b1;
    end
    else if (tx_rl) begin
      // also react on new read request, but only if the data was not yet read
      long_rdata_shifted <= 1'b0;
      if (!long_rdata_shifted) begin
        received_data_select <= received_data_select +1;
      end
    end
  end

  // select part of received data
  always_comb begin
    case (received_data_select)
      2'b00: data_received_i = data3_received_axisync;
      2'b01: data_received_i = data2_received_axisync;
      2'b10: data_received_i = data1_received_axisync;
      2'b11: data_received_i = data0_received_axisync;
      default: data_received_i = data3_received_axisync;
    endcase
  end

  // assign outputs
  assign addr_received = address_received_axisync;
  assign read_flag_received = read_flag_received_axisync;
  assign err_cmd = err_cmd_received_axisync;
  assign err_addr = err_addr_received_axisync;


  sync_pulse #(
    .WIDTH(1),
    .STAGES_SIG(1),
    .STAGES_ACK(1)
  ) sync_done_pulse_to_axi (
    .in_clk(rx_clk),
    .out_clk(axi_clk),
    .pulse_in(rx_done),
    .pulse_out(rx_done_axisync)
  );

  sync_pulse #(
    .WIDTH(1),
    .STAGES_SIG(2),
    .STAGES_ACK(1)
  ) sync_start_rx (
    .in_clk(axi_clk),
    .out_clk(rx_clk),
    .pulse_in(comm_start),
    .pulse_out(rx_start_syncrx)
  );


  H2M_sc_receiver H2M_sc_receiver_inst (
    .arst(rst),
    .rx_clk_out(rx_clk),
    .rx_start(rx_start_syncrx),
    .rx_done(rx_done),
    .rx_enabled(),
    .data0_received(rx_data0_received),
    .data1_received(rx_data1_received),
    .data2_received(rx_data2_received),
    .data3_received(rx_data3_received),
    .address_received(rx_address_received),
    .read_flag_received(rx_read_flag_received),
    .err_cmd_received(rx_err_cmd_received),
    .err_addr_received(rx_err_addr_received),
    //output err_timeout,
    .chip_SC_CLK_OUT(chip_SC_CLK_OUT),
    .chip_SC_DATA_OUT(chip_SC_DATA_OUT)
);



  //////////////////////////
  // common for rx and tx //
  //////////////////////////

  logic [10:0] timeout_counter;

  always_ff @ (posedge axi_clk) begin
    if (rst || !waiting_for_data_i || testpulse) begin
      timeout_counter <= 11'b00000000000;
    end
    else if (waiting_for_data_i) begin
      timeout_counter <= timeout_counter +1;
    end
  end

  // when writing a testpulse request, the chip responds only after the sequence is finished
  always_ff @ (posedge axi_clk) begin
    if (rst) begin
      testpulse <= 1'b0;
    end
    else if (tx_ws && addr_write == 16'h0005 && data_write[3:0] == 4'h1) begin
      testpulse <= 1'b1;
    end
    else if (rx_done_axisync) begin
      testpulse <= 1'b0;
    end
  end

  always_ff @ (posedge axi_clk) begin
    if (rst) begin
      testpulse_wait_i <= 1'b0;
    end
    else if (tx_done && testpulse) begin
      testpulse_wait_i <= 1'b1;
    end
    else if (rx_done_axisync) begin
      testpulse_wait_i <= 1'b0;
    end
  end

  assign stat_sc_idle = ~(waiting_for_data_i || set_wait_flag);

  // flag is set when rd/wr is started and reset when a response is received
  always_ff @ (posedge axi_clk) begin
    if (rst) begin
      sc_timeout <= 1'b1;
      waiting_for_data_i <= 1'b0;
    end
    else begin
      if (&timeout_counter) begin
        sc_timeout <= 1'b1;
        waiting_for_data_i <= 1'b0;
      end
      if (set_wait_flag) begin
        sc_timeout <= 1'b0;
        waiting_for_data_i <= 1'b1;
      end
      else if (rx_done_axisync) begin
        waiting_for_data_i <= 1'b0;
      end
    end
  end


  // ila_sc_interface ila_sc_interface_inst (
  //   .clk(axi_clk), // input wire clk
  //   .probe0(data_write_full), // input wire [127:0]  probe0
  //   .probe1(data_write), // input wire [31:0]  probe1
  //   .probe2(addr_write), // input wire [15:0]  probe2
  //   .probe3(addr_read), // input wire [15:0]  probe3
  //   .probe4(addr_stored), // input wire [15:0]  probe4
  //   .probe5(data_received_i), // input wire [31:0]  probe5
  //   .probe6(address_received_axisync), // input wire [15:0]  probe6
  //   .probe7(timeout_counter), // input wire [10:0]  probe7
  //   .probe8(received_data_select), // input wire [1:0]  probe8
  //   .probe9(wtx_state), // input wire [2:0]  probe9
  //   .probe10(rst), // input wire [0:0]  probe10
  //   .probe11(addr_write_wstrobe), // input wire [0:0]  probe11
  //   .probe12(addr_read_wstrobe), // input wire [0:0]  probe12
  //   .probe13(received_data_read_strobe), // input wire [0:0]  probe13
  //   .probe14(waiting_for_data_i), // input wire [0:0]  probe14
  //   .probe15(sc_timeout), // input wire [0:0]  probe15
  //   .probe16(addrw_long), // input wire [0:0]  probe16
  //   .probe17(addrr_long), // input wire [0:0]  probe17
  //   .probe18(addrw_prev_equal), // input wire [0:0]  probe18
  //   .probe19(addrr_prev_equal), // input wire [0:0]  probe19
  //   .probe20(read_flag_received_axisync), // input wire [0:0]  probe20
  //   .probe21(long_data_received_axisync), // input wire [0:0]  probe21
  //   .probe22(err_cmd_received_axisync), // input wire [0:0]  probe22
  //   .probe23(err_addr_received_axisync), // input wire [0:0]  probe23
  //   .probe24(tx_rl), // input wire [0:0]  probe24
  //   .probe25(tx_rd), // input wire [0:0]  probe25
  //   .probe26(tx_ws), // input wire [0:0]  probe26
  //   .probe27(tx_w0), // input wire [0:0]  probe27
  //   .probe28(tx_w1), // input wire [0:0]  probe28
  //   .probe29(tx_w2), // input wire [0:0]  probe29
  //   .probe30(tx_w3), // input wire [0:0]  probe30
  //   .probe31(tx_cmd_write_short_axi), // input wire [0:0]  probe31
  //   .probe32(tx_cmd_write_long_axi), // input wire [0:0]  probe32
  //   .probe33(tx_cmd_read_axi), // input wire [0:0]  probe33
  //   .probe34(set_wait_flag), // input wire [0:0]  probe34
  //   .probe35(comm_start), // input wire [0:0]  probe35
  //   .probe36(rx_done_axisync), // input wire [0:0]  probe36
  //   .probe37(long_rdata_shifted), // input wire [0:0]  probe37
  //   .probe38(testpulse), // input wire [0:0]  probe38
  //   .probe39(testpulse_wait_i) // input wire [0:0]  probe39
  // );

endmodule
