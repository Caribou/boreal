`timescale 1 ns / 1 ps

module H2M_device #(
    // Width of S_AXI data bus
    parameter integer C_S_AXI_DATA_WIDTH = 32,
    // Width of S_AXI address bus
    parameter integer C_S_AXI_ADDR_WIDTH = 14
) (
    input si_clk_p,
    input si_clk_n,

    input  TLU_trig_p,
    input  TLU_trig_n,
    input  TLU_t0_p,
    input  TLU_t0_n,
    output TLU_busy_p,
    output TLU_busy_n,

    input  gate_in,
    output debug_out,

    output chip_DAQ_CLK_IN_p,
    output chip_DAQ_CLK_IN_n,
    output chip_SHUTTER_p,  // swapped on chipboard!
    output chip_SHUTTER_n,  // swapped on chipboard!
    output chip_RESET_p,
    output chip_RESET_n,
    output chip_SC_CLK_IN_p,
    output chip_SC_CLK_IN_n,
    output chip_SC_DATA_IN_p,
    output chip_SC_DATA_IN_n,


    input chip_SC_CLK_OUT_p,
    input chip_SC_CLK_OUT_n,
    input chip_SC_DATA_OUT_p,
    input chip_SC_DATA_OUT_n,
    input chip_DIGITAL_TEST_OUT_p,
    input chip_DIGITAL_TEST_OUT_n,

    // buffer control

    output buffer_LVDS1_pwdn_p,
    output buffer_LVDS1_pwdn_n,
    output buffer_LVDS1_pem_p,
    output buffer_LVDS1_pem_n,
    output buffer_LVDS2_pwdn_p,
    output buffer_LVDS2_pwdn_n,
    output buffer_LVDS2_pem_p,
    output buffer_LVDS2_pem_n,


    // Ports of Axi4Lite Slave Bus Interface S00_AXI
    input S_AXI_ACLK,
    input S_AXI_ARESETN,
    input [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
    input [2 : 0] S_AXI_AWPROT,
    input S_AXI_AWVALID,
    output S_AXI_AWREADY,
    input [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
    input [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
    input S_AXI_WVALID,
    output S_AXI_WREADY,
    output [1 : 0] S_AXI_BRESP,
    output S_AXI_BVALID,
    input S_AXI_BREADY,
    input [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
    input [2 : 0] S_AXI_ARPROT,
    input S_AXI_ARVALID,
    output S_AXI_ARREADY,
    output [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
    output [1 : 0] S_AXI_RRESP,
    output S_AXI_RVALID,
    input S_AXI_RREADY
);


  /////////////////////////
  // AXI BUS             //
  /////////////////////////

  localparam AXI_DATA_BYTES = ((C_S_AXI_DATA_WIDTH - 1) / 8) + 1;
  localparam AXI_ADDR_LSB = ($clog2(C_S_AXI_DATA_WIDTH) - 3);
  localparam AXI_REG_ADDR_WIDTH = C_S_AXI_ADDR_WIDTH - AXI_ADDR_LSB;

  logic [C_S_AXI_DATA_WIDTH-1:0] axi_mem_rdata;
  logic [C_S_AXI_DATA_WIDTH-1:0] axi_mem_wdata;
  logic [AXI_REG_ADDR_WIDTH-1:0] axi_mem_rdAddr;
  logic [AXI_REG_ADDR_WIDTH-1:0] axi_mem_wrAddr;
  logic [AXI_DATA_BYTES-1:0] axi_mem_wrByteStrobe;
  logic axi_mem_rdStrobe;

  axi4lite_slave_interface #(
      .C_S_AXI_DATA_WIDTH(C_S_AXI_DATA_WIDTH),
      .C_S_AXI_ADDR_WIDTH(C_S_AXI_ADDR_WIDTH)
  ) axi4_slave_inst (

      .axi_mem_rdata(axi_mem_rdata),  //in
      .axi_mem_wdata(axi_mem_wdata),  //out
      .axi_mem_rdAddr(axi_mem_rdAddr),  //out
      .axi_mem_wrAddr(axi_mem_wrAddr),  //out
      .axi_mem_wrByteStrobe(axi_mem_wrByteStrobe),  //out
      //.axi_mem_rdEnable(axi_mem_rdEnable),
      .axi_mem_rdStrobe(axi_mem_rdStrobe),  //out

      .S_AXI_ACLK(S_AXI_ACLK),  //in
      .S_AXI_ARESETN(S_AXI_ARESETN),  //in
      .S_AXI_AWADDR(S_AXI_AWADDR),  //in
      .S_AXI_AWPROT(S_AXI_AWPROT),  //in
      .S_AXI_AWVALID(S_AXI_AWVALID),  //in
      .S_AXI_AWREADY(S_AXI_AWREADY),  //out
      .S_AXI_WDATA(S_AXI_WDATA),  //in
      .S_AXI_WSTRB(S_AXI_WSTRB),  //in
      .S_AXI_WVALID(S_AXI_WVALID),  //in
      .S_AXI_WREADY(S_AXI_WREADY),  //out
      .S_AXI_BRESP(S_AXI_BRESP),  //out
      .S_AXI_BVALID(S_AXI_BVALID),  //out
      .S_AXI_BREADY(S_AXI_BREADY),  //in
      .S_AXI_ARADDR(S_AXI_ARADDR),  //in
      .S_AXI_ARPROT(S_AXI_ARPROT),  //in
      .S_AXI_ARVALID(S_AXI_ARVALID),  //in
      .S_AXI_ARREADY(S_AXI_ARREADY),  //out
      .S_AXI_RDATA(S_AXI_RDATA),  //out
      .S_AXI_RRESP(S_AXI_RRESP),  //out
      .S_AXI_RVALID(S_AXI_RVALID),  //out
      .S_AXI_RREADY(S_AXI_RREADY)  //in
  );

  /////////////////////////
  // AXI FPGA REGISTERS  //
  /////////////////////////

  // number of FPGA registers
  localparam int unsigned FPGA_REGISTER_N = 17;  //
  // FPGA register mapping
  localparam byte unsigned FPGA_REG_SC_IFACE_ADDR_WRITE = 0;
  localparam byte unsigned FPGA_REG_SC_IFACE_DATA_WRITE = 1;
  localparam byte unsigned FPGA_REG_SC_IFACE_ADDR_READ = 2;
  localparam byte unsigned FPGA_REG_SC_IFACE_DATA_RECEIVED = 3;
  localparam byte unsigned FPGA_REG_SC_IFACE_STATUS = 4;
  localparam byte unsigned FPGA_REG_SC_IFACE_ADDR_RECEIVED = 5;
  localparam byte unsigned FPGA_REG_CTRL = 6;
  localparam byte unsigned FPGA_PG_CTRL = 7;
  localparam byte unsigned FPGA_PG_TIMER_PATTERN = 8;
  localparam byte unsigned FPGA_PG_TIMEOUT_PATTERN = 9;
  localparam byte unsigned FPGA_PG_OUT_PATTERN = 10;
  localparam byte unsigned FPGA_PG_TRIG_PATTERN = 11;
  localparam byte unsigned FPGA_PG_RUNS_N = 12;
  localparam byte unsigned FPGA_REG_SHUTTER = 13;
  localparam byte unsigned FPGA_REG_READOUT_DATA = 14;
  localparam byte unsigned FPGA_REG_READOUT_INFO = 15;
  localparam byte unsigned FPGA_REG_DEBUG = 16;

  logic                                axi_wren_fpgaregs;
  logic                                axi_rden_fpgaregs;
  logic [      C_S_AXI_DATA_WIDTH-1:0] fpgacfg_dout;
  logic [((C_S_AXI_DATA_WIDTH-1)/8):0] fpgacfg_wrByteStrobe[FPGA_REGISTER_N-1:0];
  logic                                fpgacfg_rdStrobe    [FPGA_REGISTER_N-1:0];
  logic [      C_S_AXI_DATA_WIDTH-1:0] fpgacfg_din         [FPGA_REGISTER_N-1:0];

  // select write to FPGA registers
  always_comb begin
    if (~|axi_mem_wrAddr[AXI_REG_ADDR_WIDTH-1:$clog2(FPGA_REGISTER_N)]) axi_wren_fpgaregs = 1'b1;
    else axi_wren_fpgaregs = 1'b0;
  end

  // select read from FPGA registers
  always_comb begin
    if (~|axi_mem_rdAddr[AXI_REG_ADDR_WIDTH-1:$clog2(FPGA_REGISTER_N)]) axi_rden_fpgaregs = 1'b1;
    else axi_rden_fpgaregs = 1'b0;
  end

  // registers demultiplexer
  mem_regs #(
      .REGISTER_N(FPGA_REGISTER_N),
      .REG_DATA_WIDTH(C_S_AXI_DATA_WIDTH)
  ) H2M_fpga_regs (
      // REG INTERFACE:
      .reg_wrdout(fpgacfg_dout),  //out
      .reg_wrByteStrobe(fpgacfg_wrByteStrobe),  //out
      .reg_rdStrobe(fpgacfg_rdStrobe),  //out
      .reg_rddin(fpgacfg_din),  //in
      // MEM INTERFACE
      .mem_wrSelect(axi_wren_fpgaregs),  //in
      .mem_rdSelect(axi_rden_fpgaregs),  //in
      .mem_rddout(axi_mem_rdata),  //out
      .mem_wrdin(axi_mem_wdata),  //in
      .mem_rdAddr(axi_mem_rdAddr[$clog2(FPGA_REGISTER_N)-1:0]),  //in
      .mem_wrAddr(axi_mem_wrAddr[$clog2(FPGA_REGISTER_N)-1:0]),  //in
      .mem_wrByteStrobe(axi_mem_wrByteStrobe),  //in
      .mem_rdStrobe(axi_mem_rdStrobe)  //in
  );

  logic sc_clk;
  logic daq_clk;
  logic global_rst;
  logic fpga_rst;
  logic chip_DAQ_CLK_IN;
  logic chip_SHUTTER;
  logic chip_RESET;
  logic chip_SC_CLK_IN;
  logic chip_SC_DATA_IN;
  logic chip_SC_CLK_OUT;
  logic chip_SC_DATA_OUT;
  logic chip_DIGITAL_TEST_OUT;

  //

  logic buffer_LVDS1_pem;
  logic buffer_LVDS1_pwdn;
  logic buffer_LVDS2_pem;
  logic buffer_LVDS2_pwdn;

  logic TLU_t0;
  logic TLU_busy;
  logic gate;
  logic debug;

  //////////////////
  // SC INTERFACE //
  //////////////////
  logic sc_ctrl_mode = 1'b0;
  //
  logic [15:0] sc_addr_write, sc_addr_write_from_axi, sc_addr_write_from_readout;
  logic [31:0] sc_data_write, sc_data_write_from_axi, sc_data_write_from_readout;
  logic
      sc_addr_write_wstrobe = 1'b0,
      sc_addr_write_wstrobe_from_axi,
      sc_addr_write_wstrobe_from_readout;
  logic [15:0] sc_addr_read, sc_addr_read_from_axi, sc_addr_read_from_readout;
  logic
      sc_addr_read_wstrobe = 1'b0, sc_addr_read_wstrobe_from_axi, sc_addr_read_wstrobe_from_readout;
  logic [15:0] sc_addr_received;
  logic [31:0] sc_data_received;
  logic sc_read_flag_received;
  logic
      sc_received_data_read_strobe,
      sc_received_data_read_strobe_from_axi,
      sc_received_data_read_strobe_from_readout;
  logic sc_stat_idle;
  logic sc_err_cmd;
  logic sc_err_addr;
  logic sc_err_timeout;
  logic sc_testpulse_wait;
  logic sc_reset;
  logic readout_reset;
  logic sc_err_axi;

  assign global_rst = fpga_rst || ~S_AXI_ARESETN;

  assign sc_err_axi = sc_ctrl_mode;

  // FPGA_REG_SC_IFACE_ADDR_WRITE
  always_ff @(posedge S_AXI_ACLK) begin
    if (global_rst) begin
      sc_addr_write_from_axi <= 16'b0;
      sc_addr_write_wstrobe_from_axi <= 1'b0;
    end else begin
      sc_addr_write_wstrobe_from_axi <= |fpgacfg_wrByteStrobe[FPGA_REG_SC_IFACE_ADDR_WRITE][1:0];
      for (byte unsigned byte_index = 0; byte_index <= 1; byte_index = byte_index + 1)
      if (fpgacfg_wrByteStrobe[FPGA_REG_SC_IFACE_ADDR_WRITE][0] == 1) begin
        sc_addr_write_from_axi[7:0] <= fpgacfg_dout[7:0];
      end
      if (fpgacfg_wrByteStrobe[FPGA_REG_SC_IFACE_ADDR_WRITE][1] == 1) begin
        sc_addr_write_from_axi[15:8] <= fpgacfg_dout[15:8];
      end
    end
  end
  assign fpgacfg_din[FPGA_REG_SC_IFACE_ADDR_WRITE][15:0]  = sc_addr_write_from_axi[15:0];
  assign fpgacfg_din[FPGA_REG_SC_IFACE_ADDR_WRITE][31:16] = 16'b0;

  // FPGA_REG_SC_IFACE_DATA_WRITE
  always_ff @(posedge S_AXI_ACLK) begin
    if (global_rst) begin
      sc_data_write_from_axi <= 32'b0;
    end else begin
      if (fpgacfg_wrByteStrobe[FPGA_REG_SC_IFACE_DATA_WRITE][0] == 1) begin
        sc_data_write_from_axi[7:0] <= fpgacfg_dout[7:0];
      end
      if (fpgacfg_wrByteStrobe[FPGA_REG_SC_IFACE_DATA_WRITE][1] == 1) begin
        sc_data_write_from_axi[15:8] <= fpgacfg_dout[15:8];
      end
      if (fpgacfg_wrByteStrobe[FPGA_REG_SC_IFACE_DATA_WRITE][2] == 1) begin
        sc_data_write_from_axi[23:16] <= fpgacfg_dout[23:16];
      end
      if (fpgacfg_wrByteStrobe[FPGA_REG_SC_IFACE_DATA_WRITE][3] == 1) begin
        sc_data_write_from_axi[31:24] <= fpgacfg_dout[31:24];
      end
    end
  end
  assign fpgacfg_din[FPGA_REG_SC_IFACE_DATA_WRITE][31:0] = sc_data_write_from_axi;

  // FPGA_REG_SC_IFACE_ADDR_READ
  always_ff @(posedge S_AXI_ACLK) begin
    if (global_rst) begin
      sc_addr_read_from_axi <= 16'b0;
      sc_addr_read_wstrobe_from_axi <= 1'b0;
    end else begin
      sc_addr_read_wstrobe_from_axi <= |fpgacfg_wrByteStrobe[FPGA_REG_SC_IFACE_ADDR_READ][1:0];
      if (fpgacfg_wrByteStrobe[FPGA_REG_SC_IFACE_ADDR_READ][0] == 1) begin
        sc_addr_read_from_axi[7:0] <= fpgacfg_dout[7:0];
      end
      if (fpgacfg_wrByteStrobe[FPGA_REG_SC_IFACE_ADDR_READ][1] == 1) begin
        sc_addr_read_from_axi[15:8] <= fpgacfg_dout[15:8];
      end
    end
  end
  assign fpgacfg_din[FPGA_REG_SC_IFACE_ADDR_READ][15:0] = sc_addr_read_from_axi[15:0];
  assign fpgacfg_din[FPGA_REG_SC_IFACE_ADDR_READ][31:16] = 16'b0;

  // FPGA_REG_SC_IFACE_DATA_READ
  assign fpgacfg_din[FPGA_REG_SC_IFACE_DATA_RECEIVED][31:0] = sc_data_received;
  assign sc_received_data_read_strobe_from_axi = fpgacfg_rdStrobe[FPGA_REG_SC_IFACE_DATA_RECEIVED];

  // FPGA_REG_SC_IFACE_STATUS
  assign fpgacfg_din[FPGA_REG_SC_IFACE_STATUS][0] = sc_stat_idle;
  assign fpgacfg_din[FPGA_REG_SC_IFACE_STATUS][1] = sc_err_cmd;
  assign fpgacfg_din[FPGA_REG_SC_IFACE_STATUS][2] = sc_err_addr;
  assign fpgacfg_din[FPGA_REG_SC_IFACE_STATUS][3] = sc_err_timeout;
  assign fpgacfg_din[FPGA_REG_SC_IFACE_STATUS][4] = sc_testpulse_wait;
  assign fpgacfg_din[FPGA_REG_SC_IFACE_STATUS][5] = sc_err_axi;
  assign fpgacfg_din[FPGA_REG_SC_IFACE_STATUS][31:6] = 25'b0;

  // FPGA_REG_SC_IFACE_ADDR_RECEIVED
  assign fpgacfg_din[FPGA_REG_SC_IFACE_ADDR_RECEIVED][15:0] = sc_addr_received;
  assign fpgacfg_din[FPGA_REG_SC_IFACE_ADDR_RECEIVED][16] = sc_read_flag_received;
  assign fpgacfg_din[FPGA_REG_SC_IFACE_ADDR_RECEIVED][31:17] = 15'b0;

  // Slow-Control block control signals multiplexing
  always_comb begin
    if (sc_ctrl_mode == 0) begin
      sc_addr_write = sc_addr_write_from_axi;
      sc_data_write = sc_data_write_from_axi;
      sc_addr_write_wstrobe = sc_addr_write_wstrobe_from_axi;
      sc_addr_read = sc_addr_read_from_axi;
      sc_addr_read_wstrobe = sc_addr_read_wstrobe_from_axi;
      sc_received_data_read_strobe = sc_received_data_read_strobe_from_axi;
    end else begin
      sc_addr_write = sc_addr_write_from_readout;
      sc_data_write = sc_data_write_from_readout;
      sc_addr_write_wstrobe = sc_addr_write_wstrobe_from_readout;
      sc_addr_read = sc_addr_read_from_readout;
      sc_addr_read_wstrobe = sc_addr_read_wstrobe_from_readout;
      sc_received_data_read_strobe = sc_received_data_read_strobe_from_readout;
    end
  end

  H2M_sc_interface H2M_sc_interface_inst (
      .tx_clk(sc_clk),
      .axi_clk(S_AXI_ACLK),
      .rst(global_rst || sc_reset),

      .addr_write(sc_addr_write),
      .data_write(sc_data_write),
      .addr_write_wstrobe(sc_addr_write_wstrobe),
      .addr_read(sc_addr_read),
      .addr_read_wstrobe(sc_addr_read_wstrobe),

      .addr_received(sc_addr_received),
      .data_received(sc_data_received),
      .read_flag_received(sc_read_flag_received),
      .received_data_read_strobe(sc_received_data_read_strobe),

      .stat_sc_idle(sc_stat_idle),
      .err_cmd(sc_err_cmd),
      .err_addr(sc_err_addr),
      .err_timeout(sc_err_timeout),
      .testpulse_wait(sc_testpulse_wait),

      .chip_SC_CLK_IN  (chip_SC_CLK_IN),
      .chip_SC_DATA_IN (chip_SC_DATA_IN),
      .chip_SC_CLK_OUT (chip_SC_CLK_OUT),
      .chip_SC_DATA_OUT(chip_SC_DATA_OUT)
  );
  //////////////////////
  // END SC INTERFACE //
  //////////////////////

  ///////////////////
  // READOUT BLOCK //
  ///////////////////

  // signals from timestamping module
  logic t0_seen;
  logic [31:0] frame_number;
  logic [47:0] ts_shutter_open;
  logic [47:0] ts_shutter_close;
  logic [47:0] ts_trigger;

  // config
  localparam [15:0] ADDR_COL_0 = 16'hA000;  //address of first column in pixel matrix
  localparam [15:0] ADDR_GLOBAL_CTRL = 16'h0001;  // GLOBAL_CTRL address

  // PG outputs:
  logic acq_control;
  logic readout_control;
  // PG trigger:
  logic readout_idle;

  logic start_acq;
  logic stop_acq;
  logic start_readout;

  logic [31:0] rd_fifo_data_out;
  logic rd_fifo_data_valid;
  logic rd_fifo_read;
  logic [31:0] fifo_info;

  //data word holder
  //logic [31:0] pixel_counter_data;
  logic readout_acq_err;

  // global_ctrl register conf
  logic [15:0] global_ctrl_readout = 16'd0;

  always @(posedge S_AXI_ACLK) begin
    if (~chip_RESET) global_ctrl_readout <= 16'd0;
    else if (sc_addr_write_wstrobe_from_axi)
      if (sc_addr_write_from_axi == ADDR_GLOBAL_CTRL) global_ctrl_readout <= sc_data_write_from_axi;
  end

  H2M_readout #(
      .ADDR_GLOBAL_CTRL(ADDR_GLOBAL_CTRL),
      .ADDR_COL_0(ADDR_COL_0)
  ) H2M_readout_inst (
      //clock and reset
      .axi_clk_i(S_AXI_ACLK),
      .reset_i  (global_rst || readout_reset),

      //readout control interface
      .acq_start_i(start_acq),
      .acq_stop_i(stop_acq),
      .start_readout_i(start_readout),
      .idle_o(readout_idle),
      .err_o(readout_acq_err),
      .busy(),

      //global_ctrl value
      .global_ctrl_i(global_ctrl_readout),

      //slow-control control interface
      .addr_write_o(sc_addr_write_from_readout),
      .data_write_o(sc_data_write_from_readout),
      .addr_write_wstrobe_o(sc_addr_write_wstrobe_from_readout),
      .addr_read_o(sc_addr_read_from_readout),
      .addr_read_wstrobe_o(sc_addr_read_wstrobe_from_readout),

      .addr_received_i(sc_addr_received),
      .data_received_i(sc_data_received),
      .read_flag_received_i(sc_read_flag_received),
      .received_data_read_strobe_o(sc_received_data_read_strobe_from_readout),

      .sc_stat_idle_i(sc_stat_idle),
      .sc_err_i(sc_err_cmd || sc_err_addr || sc_err_timeout),

      //timestamping interface input
      .t0_seen(t0_seen),
      .frame_number(frame_number),
      .ts_shutter_open(ts_shutter_open),
      .ts_shutter_close(ts_shutter_close),
      .ts_trigger(ts_trigger),

      //FIFO interface
      .fifo_data_out(rd_fifo_data_out),
      .fifo_data_valid(rd_fifo_data_valid),
      .fifo_read(rd_fifo_read),
      .fifo_info(fifo_info)
  );



  assign rd_fifo_read = fpgacfg_rdStrobe[FPGA_REG_READOUT_DATA];
  assign fpgacfg_din[FPGA_REG_READOUT_DATA][31:0] = rd_fifo_data_out;
  assign fpgacfg_din[FPGA_REG_READOUT_INFO][31:0] = fifo_info;

  ///////////////////////
  // END READOUT BLOCK //
  ///////////////////////


  ///////////////////////
  // PATTERN GENERATOR //
  ///////////////////////

  localparam byte unsigned PG_PATTERN_MEM_DEPTH = 32;
  localparam byte unsigned PG_RUN_COUNT_WIDTH = 32;
  localparam byte unsigned PG_TIMER_WIDTH = 32;
  localparam byte unsigned PG_TIMEOUT_WIDTH = 32;
  localparam byte unsigned PG_SIGNALS_N = 4;
  localparam byte unsigned PG_TRIGGERS_N = 3;

  logic [(PG_TRIGGERS_N)-1 : 0] triggers_in;  // in: trigger inputs
  logic [PG_SIGNALS_N-1 : 0] outputs_out;  // out: signals output

  logic [PG_TIMER_WIDTH-1 : 0] timer_pattern_conf;  // in: pattern time
  logic [PG_TIMEOUT_WIDTH-1 : 0] timeout_pattern_conf;
  logic [PG_SIGNALS_N-1 : 0] output_pattern_conf;  // in: pattern out
  logic [(PG_TRIGGERS_N*3)+2 : 0] triggers_pattern_conf;  // in: pattern trigger condition
  logic [PG_RUN_COUNT_WIDTH-1 : 0] conf_runs_n;  // in: number of pg runs

  logic [($clog2(PG_PATTERN_MEM_DEPTH+1))-1 : 0] mem_remaining_capacity;  // out: output
  logic stat_gen_running;  //out: status

  logic conf_run_terminate;  // in: stop after run is finished
  logic conf_run_start;  // in: start gen
  logic pg_rst;  // in: reset
  logic conf_write_pattern_in;  // in: write pattern (must be up 1 clk cycle)

  // PG trigger in signal(s)
  logic TLU_trig;
  logic tlu_trig_S;
  logic readout_idle_S;
  logic gate_S;
  logic chip_SHUTTER_gen;

  //outputs to readout control
  logic acq_control_prev = 1'b0;
  logic readout_control_prev = 1'b0;
  logic start_acq_syncdaq;
  logic stop_acq_syncdaq;
  logic start_readout_syncdaq;

  sync_signal #(
      .WIDTH (PG_TRIGGERS_N),
      .STAGES(2)
  ) sync_pg_trig (
      .out_clk(daq_clk),
      .signal_in({gate, TLU_trig, readout_idle}),
      .signal_out({gate_S, tlu_trig_S, readout_idle_S})
  );

  assign triggers_in      = {gate_S, tlu_trig_S, readout_idle_S};
  assign chip_SHUTTER_gen = outputs_out[0];
  assign acq_control      = outputs_out[1];  // H2M_readout
  assign readout_control  = outputs_out[2];  // H2M_readout
  assign TLU_busy         = outputs_out[3];  // busy flag for TLU

  always @(posedge daq_clk) begin
    readout_control_prev <= readout_control;
    acq_control_prev <= acq_control;
  end

  assign start_readout_syncdaq = ~readout_control_prev && readout_control;
  assign start_acq_syncdaq = ~acq_control_prev && acq_control;
  assign stop_acq_syncdaq = acq_control_prev && ~acq_control;

  sync_pulse #(
      .WIDTH(3),
      .STAGES_SIG(1),
      .STAGES_ACK(1)
  ) sync_start_rx (
      .in_clk   (daq_clk),
      .out_clk  (S_AXI_ACLK),
      .pulse_in ({start_readout_syncdaq, start_acq_syncdaq, stop_acq_syncdaq}),
      .pulse_out({start_readout, start_acq, stop_acq})
  );

  pattern_generator #(
      .PG_PATTERN_MEM_DEPTH(PG_PATTERN_MEM_DEPTH),
      .PG_RUN_COUNT_WIDTH(PG_RUN_COUNT_WIDTH),
      .PG_TIMER_WIDTH(PG_TIMER_WIDTH),
      .PG_SIGNALS_N(PG_SIGNALS_N),
      .PG_TRIGGERS_N(PG_TRIGGERS_N)
  ) pattern_generator_inst (
      .clk_cfg(S_AXI_ACLK),
      .clk_gen(daq_clk),  // time unit
      .rst(global_rst || pg_rst),

      .triggers_in(triggers_in),  //
      .outputs_out(outputs_out),  //

      .timer_pattern_conf_in(timer_pattern_conf),  //
      .timeout_pattern_conf_in(timeout_pattern_conf),  //
      .output_pattern_conf_in(output_pattern_conf),  //
      .triggers_pattern_conf_in(triggers_pattern_conf),  //
      .conf_write_pattern_in(conf_write_pattern_in),  //

      .mem_remaining_capacity(mem_remaining_capacity),  //
      .conf_runs_n(conf_runs_n),  //
      .conf_run_terminate(conf_run_terminate),  //
      .conf_run_start(conf_run_start),  //
      .stat_gen_running(stat_gen_running)  //
  );

  // PG control register
  // FPGA_PG_CTRL
  always_ff @(posedge S_AXI_ACLK) begin
    if (global_rst) begin
      conf_run_start        <= 1'b0;
      conf_run_terminate    <= 1'b0;
      pg_rst                <= 1'b0;
      conf_write_pattern_in <= 1'b0;
    end else begin
      conf_run_start        <= 1'b0;
      conf_run_terminate    <= 1'b0;
      pg_rst                <= 1'b0;
      conf_write_pattern_in <= 1'b0;
      if (fpgacfg_wrByteStrobe[FPGA_PG_CTRL][0] == 1) begin
        conf_run_start        <= fpgacfg_dout[0];
        conf_run_terminate    <= fpgacfg_dout[1];
        pg_rst                <= fpgacfg_dout[2];
        conf_write_pattern_in <= fpgacfg_dout[3];
      end
    end
  end
  // PG_CTRL:
  // [0] conf_run_start - wr only
  // [1] conf_run_terminate - wr only
  // [2] pg_rst - wr only
  // [3] conf_write_pattern_in - wr only
  // ...
  // [8] stat_gen_running - rd only
  // ...
  // [21:16] mem_remaining_capacity - rd only
  assign fpgacfg_din[FPGA_PG_CTRL][7:0] = 8'b0;  // wr only & empty
  assign fpgacfg_din[FPGA_PG_CTRL][8] = stat_gen_running;
  assign fpgacfg_din[FPGA_PG_CTRL][15:9] = 7'b0;  // empty
  assign fpgacfg_din[FPGA_PG_CTRL][($clog2(PG_PATTERN_MEM_DEPTH+1))+15:16] = mem_remaining_capacity;

  // FPGA_PG_TIMER_PATTERN, FPGA_PG_OUT_PATTERN, FPGA_PG_TRIG_PATTERN, FPGA_PG_RUNS_N
  always_ff @(posedge S_AXI_ACLK) begin
    if (global_rst) begin
      timer_pattern_conf    <= 32'b0;
      timeout_pattern_conf  <= 32'b0;
      output_pattern_conf   <= 32'b0;
      triggers_pattern_conf <= 32'b0;
      conf_runs_n           <= 32'b0;
    end else begin

      // FPGA_PG_TIMER_PATTERN
      for (
          byte unsigned byte_index = 0;
          byte_index <= AXI_DATA_BYTES - 1;
          byte_index = byte_index + 1
      )
      if (fpgacfg_wrByteStrobe[FPGA_PG_TIMER_PATTERN][byte_index] == 1)
        timer_pattern_conf[(byte_index*8)+:8] <= fpgacfg_dout[(byte_index*8)+:8];

      // FPGA_PG_TIMEOUT_PATTERN
      for (
          byte unsigned byte_index = 0;
          byte_index <= AXI_DATA_BYTES - 1;
          byte_index = byte_index + 1
      )
      if (fpgacfg_wrByteStrobe[FPGA_PG_TIMEOUT_PATTERN][byte_index] == 1)
        timeout_pattern_conf[(byte_index*8)+:8] <= fpgacfg_dout[(byte_index*8)+:8];

      // FPGA_PG_OUT_PATTERN - [PG_SIGNALS_N-1 : 0]
      if (fpgacfg_wrByteStrobe[FPGA_PG_OUT_PATTERN][0] == 1) begin
        output_pattern_conf[(PG_SIGNALS_N-1):0] <= fpgacfg_dout[(PG_SIGNALS_N-1):0];
      end

      // FPGA_PG_TRIG_PATTERN - [(PG_TRIGGERS_N*3)+2 : 0]
      if (fpgacfg_wrByteStrobe[FPGA_PG_TRIG_PATTERN][0] == 1) begin
        if ((PG_TRIGGERS_N * 3) + 2 < 8) begin
          triggers_pattern_conf[(PG_TRIGGERS_N*3)+2:0] <= fpgacfg_dout[(PG_TRIGGERS_N*3)+2:0];
        end else begin
          triggers_pattern_conf[7:0] <= fpgacfg_dout[7:0];
        end
      end
      if (fpgacfg_wrByteStrobe[FPGA_PG_TRIG_PATTERN][1] == 1) begin
        if ((PG_TRIGGERS_N * 3) + 2 > 7) begin
          triggers_pattern_conf[(PG_TRIGGERS_N*3)+2:8] <= fpgacfg_dout[(PG_TRIGGERS_N*3)+2:8];
        end
      end

      //FPGA_PG_RUNS_N
      for (
          byte unsigned byte_index = 0;
          byte_index <= AXI_DATA_BYTES - 1;
          byte_index = byte_index + 1
      )
      if (fpgacfg_wrByteStrobe[FPGA_PG_RUNS_N][byte_index] == 1)
        conf_runs_n[(byte_index*8)+:8] <= fpgacfg_dout[(byte_index*8)+:8];

    end
  end

  assign fpgacfg_din[FPGA_PG_TIMER_PATTERN][31:0]   = timer_pattern_conf;
  assign fpgacfg_din[FPGA_PG_TIMEOUT_PATTERN][31:0] = timeout_pattern_conf;
  assign fpgacfg_din[FPGA_PG_OUT_PATTERN][31:0]     = output_pattern_conf;
  assign fpgacfg_din[FPGA_PG_TRIG_PATTERN][31:0]    = triggers_pattern_conf;
  assign fpgacfg_din[FPGA_PG_RUNS_N][31:0]          = conf_runs_n;

  ///////////////////////////
  // END PATTERN GENERATOR //
  ///////////////////////////

  ////////////////
  // TIMESTAMPS //
  ////////////////

  logic conf_ts_start;

  H2M_timestamps #(
      .TS_WIDTH(48),
      .FRAME_NO_WIDTH(32)
  ) H2M_timestamps_inst (
      .daq_clk(daq_clk),  // i
      .axi_clk(S_AXI_ACLK),  // i
      .arst(global_rst),  // i
      .t0_async(TLU_t0),  // i
      .shutter_syncdaq(chip_SHUTTER),  // i
      .trigger_syncdaq(tlu_trig_S),  // i
      .conf_ts_start_syncaxi(conf_ts_start),  // i
      .t0_seen_syncaxi(t0_seen),  // o
      .frame_number_syncaxi(frame_number),  // o [FRAME_NO_WIDTH-1:0]
      .ts_shutter_open_syncaxi(ts_shutter_open),  // o [TS_WIDTH-1:0]
      .ts_shutter_close_syncaxi(ts_shutter_close),  // o [TS_WIDTH-1:0]
      .ts_trigger_syncaxi(ts_trigger)  // o [TS_WIDTH-1:0]
  );


  //////////////////////
  // GENERAL CONTROLS //
  //////////////////////

  // FPGA_REG_CTRL
  always_ff @(posedge S_AXI_ACLK) begin
    if (global_rst) begin
      chip_RESET        <= 1'b0;
      fpga_rst          <= 1'b0;
      readout_reset     <= 1'b0;
      sc_reset          <= 1'b0;
      conf_ts_start     <= 1'b0;
      sc_ctrl_mode      <= 1'b0;
      buffer_LVDS1_pwdn <= 1'b0;
      buffer_LVDS1_pem  <= 1'b0;
      buffer_LVDS2_pwdn <= 1'b0;
      buffer_LVDS2_pem  <= 1'b0;
    end else begin
      fpga_rst      <= 1'b0;
      readout_reset <= 1'b0;
      sc_reset      <= 1'b0;
      conf_ts_start <= 1'b0;
      if (fpgacfg_wrByteStrobe[FPGA_REG_CTRL][0] == 1) begin
        chip_RESET    <= fpgacfg_dout[0];
        fpga_rst      <= fpgacfg_dout[1];
        readout_reset <= fpgacfg_dout[2];
        sc_reset      <= fpgacfg_dout[3];
        conf_ts_start <= fpgacfg_dout[4];
        sc_ctrl_mode  <= fpgacfg_dout[5];
      end
      if (fpgacfg_wrByteStrobe[FPGA_REG_CTRL][1] == 1) begin
        buffer_LVDS1_pwdn <= fpgacfg_dout[8];
        buffer_LVDS1_pem  <= fpgacfg_dout[9];
        buffer_LVDS2_pwdn <= fpgacfg_dout[10];
        buffer_LVDS2_pem  <= fpgacfg_dout[11];
      end
    end
  end

  assign fpgacfg_din[FPGA_REG_CTRL][0] = chip_RESET;
  assign fpgacfg_din[FPGA_REG_CTRL][4:1] = 'b0;
  assign fpgacfg_din[FPGA_REG_CTRL][5] = sc_ctrl_mode;
  assign fpgacfg_din[FPGA_REG_CTRL][7:6] = 'b0;
  assign fpgacfg_din[FPGA_REG_CTRL][8] = buffer_LVDS1_pwdn;
  assign fpgacfg_din[FPGA_REG_CTRL][9] = buffer_LVDS1_pem;
  assign fpgacfg_din[FPGA_REG_CTRL][10] = buffer_LVDS2_pwdn;
  assign fpgacfg_din[FPGA_REG_CTRL][11] = buffer_LVDS2_pem;

  assign fpgacfg_din[FPGA_REG_CTRL][15:12] = 'b0;
  assign fpgacfg_din[FPGA_REG_CTRL][16] = readout_idle;
  assign fpgacfg_din[FPGA_REG_CTRL][17] = readout_acq_err;
  assign fpgacfg_din[FPGA_REG_CTRL][31:18] = 'b0;

  // DEBUG
  logic [2:0] debug_sw;

  always_ff @(posedge S_AXI_ACLK) begin
    if (global_rst) begin
      debug_sw <= 'b0;
    end else begin
      if (fpgacfg_wrByteStrobe[FPGA_REG_DEBUG][0] == 1) begin
        debug_sw <= fpgacfg_dout[2:0];
      end
    end
  end
  assign fpgacfg_din[FPGA_REG_DEBUG][2:0] = debug_sw;

  always_comb begin
    case (debug_sw)
      0: debug = readout_idle;
      1: debug = readout_acq_err;
      2: debug = sc_err_cmd;
      3: debug = sc_err_addr;
      4: debug = sc_err_timeout;
      default: debug = 1'b0;
    endcase
  end
  ////////////////////
  // MANUAL SHUTTER //
  ////////////////////

  logic [31:0] chip_SHUTTER_length;
  logic [31:0] chip_SHUTTER_time;
  logic chip_SHUTTER_man;
  logic chip_SHUTTER_trg = 1'b0;

  //FPGA_REG_SHUTTER
  always_ff @(posedge S_AXI_ACLK) begin
    if (global_rst) begin
      chip_SHUTTER_length <= 32'b0;
      chip_SHUTTER_trg <= 1'b0;
    end else begin
      chip_SHUTTER_trg <= |fpgacfg_wrByteStrobe[FPGA_REG_SHUTTER];
      for (
          byte unsigned byte_index = 0;
          byte_index <= AXI_DATA_BYTES - 1;
          byte_index = byte_index + 1
      )
      if (fpgacfg_wrByteStrobe[FPGA_REG_SHUTTER][byte_index] == 1)
        chip_SHUTTER_length[(byte_index*8)+:8] <= fpgacfg_dout[(byte_index*8)+:8];
    end
  end
  assign fpgacfg_din[FPGA_REG_SHUTTER][31:0] = chip_SHUTTER_time;


  H2M_shutter H2M_shutter_inst (
      .axi_clk(S_AXI_ACLK),
      .daq_clk(daq_clk),
      .arst(global_rst),
      .shutter_trg(chip_SHUTTER_trg),
      .shutter_length_in(chip_SHUTTER_length),
      .shutter_time_out(chip_SHUTTER_time),
      .shutter_out(chip_SHUTTER_man)
  );

  assign chip_DAQ_CLK_IN = daq_clk;
  assign chip_SHUTTER = chip_SHUTTER_man || chip_SHUTTER_gen;

  //////////////
  // CLOCKING //
  //////////////

  H2M_clocking H2M_clocking_inst (
      .si_clk_p(si_clk_p),
      .si_clk_n(si_clk_n),
      .daq_clk(daq_clk),
      .sc_clk(sc_clk),
      .reset(global_rst),
      .locked()
  );

  ////////////
  // BUFFERS //
  ////////////
  IBUF ibuf_gate_in (
      .I(gate_in),
      .O(gate)
  );

  OBUF obuf_debug_out (
      .I(debug),
      .O(debug_out)
  );

  IBUFDS ibufds_tlu_trig (
      .I (TLU_trig_p),
      .IB(TLU_trig_n),
      .O (TLU_trig)
  );

  IBUFDS ibufds_tlu_t0 (
      .I (TLU_t0_p),
      .IB(TLU_t0_n),
      .O (TLU_t0)
  );

  OBUFDS obufds_TLU_busy (
      .O (TLU_busy_p),
      .OB(TLU_busy_n),
      .I (TLU_busy)
  );

  OBUFDS obufds_chip_DAQ_CLK_IN (
      .O (chip_DAQ_CLK_IN_p),
      .OB(chip_DAQ_CLK_IN_n),
      .I (chip_DAQ_CLK_IN)
  );

  OBUFDS obufds_chip_SHUTTER (
      .O (chip_SHUTTER_p),
      .OB(chip_SHUTTER_n),
      .I (chip_SHUTTER)
  );

  OBUFDS obufds_chip_RESET (
      .O (chip_RESET_p),
      .OB(chip_RESET_n),
      .I (chip_RESET)
  );

  OBUFDS obufds_chip_SC_CLK_IN (
      .O (chip_SC_CLK_IN_p),
      .OB(chip_SC_CLK_IN_n),
      .I (chip_SC_CLK_IN)
  );

  OBUFDS obufds_chip_SC_DATA_IN (
      .O (chip_SC_DATA_IN_p),
      .OB(chip_SC_DATA_IN_n),
      .I (chip_SC_DATA_IN)
  );

  IBUFDS ibufds_chip_SC_CLK_OUT (
      .I (chip_SC_CLK_OUT_p),
      .IB(chip_SC_CLK_OUT_n),
      .O (chip_SC_CLK_OUT)
  );

  IBUFDS ibufds_chip_SC_DATA_OUT (
      .I (chip_SC_DATA_OUT_p),
      .IB(chip_SC_DATA_OUT_n),
      .O (chip_SC_DATA_OUT)
  );

  IBUFDS ibufds_chip_DIGITAL_TEST_OUT (
      .I (chip_DIGITAL_TEST_OUT_p),
      .IB(chip_DIGITAL_TEST_OUT_n),
      .O (chip_DIGITAL_TEST_OUT)
  );

  // LVDS buffers control

  OBUFDS obufds_buffer_LVDS1_pem (
      .O (buffer_LVDS1_pem_p),
      .OB(buffer_LVDS1_pem_n),
      .I (buffer_LVDS1_pem)
  );

  OBUFDS obufds_buffer_LVDS1_pwdn (
      .O (buffer_LVDS1_pwdn_p),
      .OB(buffer_LVDS1_pwdn_n),
      .I (buffer_LVDS1_pwdn)
  );

  OBUFDS obufds_buffer_LVDS2_pem (
      .O (buffer_LVDS2_pem_p),
      .OB(buffer_LVDS2_pem_n),
      .I (buffer_LVDS2_pem)
  );

  OBUFDS obufds_buffer_LVDS2_pwdn (
      .O (buffer_LVDS2_pwdn_p),
      .OB(buffer_LVDS2_pwdn_n),
      .I (buffer_LVDS2_pwdn)
  );

  /////////
  // ILA //
  /////////

  // ila_device ila_device_inst (
  //     .clk(S_AXI_ACLK),  // input wire clk
  //     .probe0(global_rst),  // input wire [0:0]  probe0
  //     .probe1(chip_SHUTTER_length),  // input wire [31:0]  probe1
  //     .probe2(chip_SHUTTER_time),  // input wire [31:0]  probe2
  //     .probe3(chip_SHUTTER_trg),  // input wire [0:0]  probe3
  //     .probe4(chip_SHUTTER_man),  // input wire [0:0]  probe4
  //     .probe5(chip_SHUTTER),  // input wire [0:0]  probe5
  //     .probe6(gate)  // input wire [0:0]  probe6

  // );

endmodule
