module H2M_sc_transmitter (
    input  tx_clk,
    input  arst,

    input  [15:0] tx_address,
    input  [127:0] tx_data,
    input  tx_cmd_write_short,
    input  tx_cmd_write_long,
    input  tx_cmd_read,
    output tx_done,
    output tx_busy,

    output chip_SC_CLK_IN,
    output chip_SC_DATA_IN
);

  logic [167:0] tx_shr = 168'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
  // mapping:
  // [7:0] sync_hdr = tx_shr[167:160]
  //[14:0] zeroes   = tx_shr[159:145]
  //    tx_read_bit = tx_shr[144]
  //[15:0] tx_addr  = tx_shr[143:128]
  //[31:0] tx_data3 = tx_shr[127:96]
  //[31:0] tx_data2 = tx_shr[95:64]
  //[31:0] tx_data1 = tx_shr[63:32]
  //[31:0] tx_data0 = tx_shr[31:0]

  logic busy = 1'b0;
  logic tx_start_read = 1'b0;
  logic tx_start_wr_16 = 1'b0;
  logic tx_start_wr_128 = 1'b0;
  logic [7:0] tx_bitcounter = 8'h00;
  logic tx_shift;
  logic tx_done_i = 1'b0;

  assign chip_SC_DATA_IN = tx_shr[167];
  assign chip_SC_CLK_IN  = tx_clk;

  assign tx_done = tx_done_i;
  assign tx_busy = busy;


  sync_reset #(
    .NEGATIVE(0),
    .STAGES(2)
  ) sync_tx_rst (
    .clk(tx_clk),
    .arst_in(arst),
    .rst_out(tx_rst)
  );


  always_ff @ (posedge tx_clk) begin
    // these need to be de-asserted every clock cycle otherwise the bitcounter doesn't run
    tx_start_read   <= 1'b0;
    tx_start_wr_16  <= 1'b0;
    tx_start_wr_128 <= 1'b0;

    if (tx_rst) begin
      // keep idle stat at '1'
      tx_shr        <= 168'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF;
    end
    else if (~busy) begin // only start when not busy
      if (tx_cmd_read) begin // reading register
        tx_shr[167:160] <= 8'hAA; // sync
        tx_shr[159:145] <= 15'b000000000000000; // zeroes
        tx_shr[144]     <= 1'b1; // read flag set
        tx_shr[143:128] <= tx_address; // register address
        tx_shr[127:0]   <= 128'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF; // empty
        busy            <= 1'b1;
        tx_start_read   <= 1'b1;
      end
      else if (tx_cmd_write_short) begin // write 16b register
        tx_shr[167:160] <= 8'hAA; // sync
        tx_shr[159:145] <= 15'b000000000000000; // zeroes
        tx_shr[144]     <= 1'b0; // read flag cleared
        tx_shr[143:128] <= tx_address; // register address
        tx_shr[127:112] <= tx_data[15:0]; // 16b data
        tx_shr[111:0]   <= 112'hFFFFFFFFFFFFFFFFFFFFFFFFFFFF; // empty
        busy            <= 1'b1;
        tx_start_wr_16  <= 1'b1;
      end
      else if (tx_cmd_write_long) begin
        tx_shr[167:160] <= 8'hAA; // sync
        tx_shr[159:145] <= 15'b000000000000000; // zeroes
        tx_shr[144]     <= 1'b0; // read flag cleared
        tx_shr[143:128] <= tx_address; // register address
        tx_shr[127:0]   <= tx_data; // 128b data
        busy            <= 1'b1;
        tx_start_wr_128 <= 1'b1;
      end
    end
    // not busy and request to shift data out
    else if (tx_shift) begin
      tx_shr[167:0] <= {tx_shr[166:0], 1'b1};
    end
    else begin
      if (tx_done_i) begin
        busy <= 1'b0;
      end
    end
  end

  // count how many bits to shift out from the tx shift register
  assign tx_shift = |tx_bitcounter;
  always_ff @ (posedge tx_clk) begin
    // reset
    if (tx_rst) tx_bitcounter <= 8'h00;
    // 8b(sync) + 15b(zeroes) + 1b(read flag) + 16b(address) == 40b
    else if (tx_start_read)  tx_bitcounter <= 8'h28;
    // 8b(sync) + 15b(zeroes) + 1b(read flag) + 16b(address) + 16b(data) = 56b
    else if (tx_start_wr_16) tx_bitcounter <= 8'h38;
    // 8b(sync) + 15b(zeroes) + 1b(read flag) + 16b(address) + 128b(data) == 168b
    else if (tx_start_wr_128)  tx_bitcounter <= 8'hA8;
    // count
    else if (tx_shift)  tx_bitcounter <= tx_bitcounter -1;
  end

  always_ff @ (posedge tx_clk) begin
    tx_done_i <= 1'b0;
    if (tx_bitcounter == 1) tx_done_i <= 1'b1;
  end

  // ila_sc_transmitter ila_sc_transmitter_inst (
  //   .clk(tx_clk), // input wire clk
  //   .probe0(tx_shr), // input wire [167:0]  probe0
  //   .probe1(tx_bitcounter), // input wire [7:0]  probe1
  //   .probe2(busy), // input wire [0:0]  probe2
  //   .probe3(tx_start_read), // input wire [0:0]  probe3
  //   .probe4(tx_start_wr_16), // input wire [0:0]  probe4
  //   .probe5(tx_start_wr_128), // input wire [0:0]  probe5
  //   .probe6(tx_shift), // input wire [0:0]  probe6
  //   .probe7(tx_done_i), // input wire [0:0]  probe7
  //   .probe8(tx_cmd_write_short), // input wire [0:0]  probe8
  //   .probe9(tx_cmd_write_long), // input wire [0:0]  probe9
  //   .probe10(tx_cmd_read) // input wire [0:0]  probe10
  // );


endmodule
