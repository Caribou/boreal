module H2M_shutter(
    input  axi_clk,
    input  daq_clk,
    input  arst,
    input  shutter_trg,
    input  [31:0] shutter_length_in,
    output [31:0] shutter_time_out,
    output shutter_out
  );

  logic rst;
  logic [31:0] shutter_length_sync;
  logic [31:0] shutter_timer;
  logic shutter_load;
  logic shutter_i = 1'b0;

  assign shutter_out = shutter_i;

  sync_reset #(
    .NEGATIVE(0),
    .STAGES(5)
  ) sync_shutter_rst (
    .clk(daq_clk),
    .arst_in(arst),
    .rst_out(rst)
  );

  sync_bus #(
    .WIDTH(32),
    .STAGES_SIG(1),
    .STAGES_ACK(1)
  ) sync_bus_shutter_timer (
    .in_clk(daq_clk),
    .out_clk(axi_clk),
    .bus_in(shutter_timer),
    .bus_out(shutter_time_out)
  );

  sync_bus_with_strobe #(
    .WIDTH(32),
    .STAGES_SIG(2),
    .STAGES_ACK(1)
  ) sync_bus_shutter_length (
    .in_clk(axi_clk),
    .out_clk(daq_clk),
    .in_strobe(shutter_trg),
    .out_strobe(shutter_load),
    .bus_in(shutter_length_in),
    .bus_out(shutter_length_sync)
  );


  always_ff @ (posedge daq_clk) begin
    if (rst) begin
      shutter_timer <= 32'h0;
    end
    else if (shutter_load) begin
      shutter_timer <= shutter_length_sync;
    end
    else if (|shutter_timer) begin
      shutter_timer <= shutter_timer -1;
    end
  end

  always_ff @ (posedge daq_clk) begin
    if (rst) begin
      shutter_i <= 1'b0;
    end
    else begin
      shutter_i <= |shutter_timer;
    end
  end


  // ila_shutter ila_shutter_inst (
  //   .clk(daq_clk), // input wire clk
  //   .probe0(rst), // input wire [0:0]  probe0
  //   .probe1(shutter_length_sync), // input wire [31:0]  probe1
  //   .probe2(shutter_timer), // input wire [31:0]  probe2
  //   .probe3(shutter_load), // input wire [0:0]  probe3
  //   .probe4(shutter_i) // input wire [0:0]  probe4
  // );


endmodule
