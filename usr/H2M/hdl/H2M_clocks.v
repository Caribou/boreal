`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 07/24/2023 12:27:32 AM
// Design Name:
// Module Name: H2M_clocking
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module H2M_clocking(
    input  si_clk_p,
    input  si_clk_n,
    output daq_clk,
    output sc_clk,
    input  reset,
    output locked
  );

  wire si_clk_i;
  wire si_clk;
  IBUFDS ibufds_si_clk (
    .O  (si_clk_i),
    .I  (si_clk_p),
    .IB (si_clk_n)
  );

  BUFG bufg_si_clk (
    .O (si_clk),
    .I (si_clk_i)
  );

  assign daq_clk = si_clk;

  // Clocking PRIMITIVE
  //------------------------------------

  // Instantiation of the MMCM PRIMITIVE
  //    * Unused inputs are tied off
  //    * Unused outputs are labeled unused

  wire        daq_i_clk;
  wire        sc_i_clk;
  wire        fbout_clk;
  wire        fbbuf_clk;
  wire        locked_int;
  wire        reset_high;

  wire [15:0] do_unused;
  wire        drdy_unused;
  wire        clkout2_unused;
  wire        clkout3_unused;
  wire        clkout4_unused;
  wire        clkout5_unused;


  PLLE2_ADV #(
    .BANDWIDTH            ("OPTIMIZED"),
    .COMPENSATION         ("ZHOLD"),
    .STARTUP_WAIT         ("FALSE"),
    .DIVCLK_DIVIDE        (1),
    .CLKFBOUT_MULT        (10),
    .CLKFBOUT_PHASE       (0.000),
    .CLKOUT0_DIVIDE       (25),
    .CLKOUT0_PHASE        (0.000),
    .CLKOUT0_DUTY_CYCLE   (0.500),
    //.CLKOUT1_DIVIDE       (10),
    //.CLKOUT1_PHASE        (0.000),
    //.CLKOUT1_DUTY_CYCLE   (0.500),
    .CLKIN1_PERIOD        (10.000)
  ) plle2_adv_inst (
   // Output clocks
    .CLKFBOUT            (fbout_clk),
    .CLKOUT0             (sc_i_clk),
    .CLKOUT1             (daq_i_clk),
    .CLKOUT2             (clkout2_unused),
    .CLKOUT3             (clkout3_unused),
    .CLKOUT4             (clkout4_unused),
    .CLKOUT5             (clkout5_unused),
     // Input clock control
    .CLKFBIN             (fbbuf_clk),
    .CLKIN1              (si_clk),
    .CLKIN2              (1'b0),
     // Tied to always select the primary input clock
    .CLKINSEL            (1'b1),
    // Ports for dynamic reconfiguration
    .DADDR               (7'h0),
    .DCLK                (1'b0),
    .DEN                 (1'b0),
    .DI                  (16'h0),
    .DO                  (do_unused),
    .DRDY                (drdy_unused),
    .DWE                 (1'b0),
    // Other control and status signals
    .LOCKED              (locked_int),
    .PWRDWN              (1'b0),
    .RST                 (reset_high)
  );

  assign reset_high = reset;
  assign locked = locked_int;

  // Output buffering
  //-----------------------------------

  BUFG fbclk_buf (
    .O (fbbuf_clk),
    .I (fbout_clk)
  );

  BUFG sc_clk_buf (
    .O   (sc_clk),
    .I   (sc_i_clk)
  );

  //BUFG daq_clk_buf (
  //  .O   (daq_clk),
  //  .I   (daq_i_clk)
  //);

endmodule
