#set_property DIFF_TERM_ADV TERM_100 [get_ports ]
#set_property -dict {PACKAGE_PIN    IOSTANDARD LVCMOS25  } [get_ports {}]
#set_property -dict {PACKAGE_PIN    IOSTANDARD LVDS  } [get_ports {}]

## Clocks input
# SI5345_CLK_OUT8_{p,n}, HPC FMC {H4, H5}
set_property -dict {PACKAGE_PIN AA7 IOSTANDARD LVDS} [get_ports si_clk_p]
set_property -dict {PACKAGE_PIN AA6 IOSTANDARD LVDS} [get_ports si_clk_n]
set_property DIFF_TERM_ADV TERM_100 [get_ports si_clk_*]

# FMC_REF_CLK_{p,n} HPC FMC {G21, G22}
# Reference clock to Si5345, in ch. 0 (output)
set_property PACKAGE_PIN N13 [get_ports fmc_ref_clk_p]
set_property PACKAGE_PIN M13 [get_ports fmc_ref_clk_n]
set_property IOSTANDARD LVDS [get_ports fmc_ref_clk_*]
#set_property DIFF_TERM_ADV TERM_100     [get_ports fmc_ref_clk_*]

## TLU connections
# TLU_trig_{p,n} HPC FMC {D11, D12}
# Trigger (input)
set_property PACKAGE_PIN AB3 [get_ports TLU_trig_p]
set_property PACKAGE_PIN AC3 [get_ports TLU_trig_n]
set_property IOSTANDARD LVDS [get_ports TLU_trig_*]
set_property DIFF_TERM_ADV TERM_100 [get_ports TLU_trig_*]

# TLU_RST_{p,n} HPC FMC {D17, D18}
# T0 (input)
set_property PACKAGE_PIN AB8 [get_ports TLU_t0_p]
set_property PACKAGE_PIN AC8 [get_ports TLU_t0_n]
set_property IOSTANDARD LVDS [get_ports TLU_t0_*]
set_property DIFF_TERM_ADV TERM_100 [get_ports TLU_t0_*]

# TLU_BUSY_{p,n} HPC FMC {D14, D15}
# Detector busy flag (output)
set_property PACKAGE_PIN W2 [get_ports TLU_busy_p]
set_property PACKAGE_PIN W1 [get_ports TLU_busy_n]
set_property IOSTANDARD LVDS [get_ports TLU_busy_*]
#set_property DIFF_TERM_ADV TERM_100 [get_ports TLU_bsy_*]

# TLU_CLK_{p,n} HPC FMC {D8, D9}
# 40 MHz TLU clock (input)
set_property PACKAGE_PIN AB4 [get_ports TLU_clk_p]
set_property PACKAGE_PIN AC4 [get_ports TLU_clk_n]
set_property IOSTANDARD LVDS [get_ports TLU_clk_*]
# set_property DIFF_TERM_ADV TERM_100 [get_ports TLU_clk_*]

# FIXME no USER_SMA_CLOCK connectors on ZCU102
# USER_SMA_CLOCK_P for gating
# set_property -dict {PACKAGE_PIN N27 } [get_ports USER_SMA_CLOCK_P]
set_property -dict {IOSTANDARD LVCMOS18} [get_ports USER_SMA_CLOCK_P]
# debug SMA pin
# set_property -dict {PACKAGE_PIN N28} [get_ports USER_SMA_CLOCK_N]
set_property -dict {IOSTANDARD LVCMOS18 SLEW FAST} [get_ports USER_SMA_CLOCK_N]

## H2M chip connections
## FPGA -> chip
set_property -dict {PACKAGE_PIN Y10 IOSTANDARD LVDS} [get_ports chip_DAQ_CLK_IN_p]
set_property -dict {PACKAGE_PIN Y9 IOSTANDARD LVDS} [get_ports chip_DAQ_CLK_IN_n]
set_property -dict {PACKAGE_PIN P11 IOSTANDARD LVDS} [get_ports chip_SHUTTER_p]
set_property -dict {PACKAGE_PIN N11 IOSTANDARD LVDS} [get_ports chip_SHUTTER_n]
set_property -dict {PACKAGE_PIN Y12 IOSTANDARD LVDS} [get_ports chip_RESET_p]
set_property -dict {PACKAGE_PIN AA12 IOSTANDARD LVDS} [get_ports chip_RESET_n]
set_property -dict {PACKAGE_PIN Y4 IOSTANDARD LVDS} [get_ports chip_SC_CLK_IN_p]
set_property -dict {PACKAGE_PIN Y3 IOSTANDARD LVDS} [get_ports chip_SC_CLK_IN_n]
set_property -dict {PACKAGE_PIN AC7 IOSTANDARD LVDS} [get_ports chip_SC_DATA_IN_p]
set_property -dict {PACKAGE_PIN AC6 IOSTANDARD LVDS} [get_ports chip_SC_DATA_IN_n]
## chip -> FPGA
set_property -dict {PACKAGE_PIN AB6 IOSTANDARD LVDS} [get_ports chip_SC_CLK_OUT_p]
set_property -dict {PACKAGE_PIN AB5 IOSTANDARD LVDS} [get_ports chip_SC_CLK_OUT_n]
set_property DIFF_TERM_ADV TERM_100 [get_ports chip_SC_CLK_OUT_*]
set_property -dict {PACKAGE_PIN Y2 IOSTANDARD LVDS} [get_ports chip_SC_DATA_OUT_p]
set_property -dict {PACKAGE_PIN Y1 IOSTANDARD LVDS} [get_ports chip_SC_DATA_OUT_n]
set_property DIFF_TERM_ADV TERM_100 [get_ports chip_SC_DATA_OUT_*]
set_property -dict {PACKAGE_PIN AA2 IOSTANDARD LVDS} [get_ports chip_DIGITAL_TEST_OUT_p]
set_property -dict {PACKAGE_PIN AA1 IOSTANDARD LVDS} [get_ports chip_DIGITAL_TEST_OUT_n]
set_property DIFF_TERM_ADV TERM_100 [get_ports chip_DIGITAL_TEST_OUT*]

## H2M chip board buffers
#FIXME
# set_property -dict {PACKAGE_PIN Y26 IOSTANDARD LVDS} [get_ports buffer_LVDS1_pwdn_p]
set_property -dict {IOSTANDARD LVDS} [get_ports buffer_LVDS1_pwdn_p]
# set_property -dict {PACKAGE_PIN Y27 IOSTANDARD LVDS} [get_ports buffer_LVDS1_pwdn_n]
set_property -dict {IOSTANDARD LVDS} [get_ports buffer_LVDS1_pwdn_n]

set_property -dict {PACKAGE_PIN T7 IOSTANDARD LVDS} [get_ports buffer_LVDS1_pem_p]
set_property -dict {PACKAGE_PIN T6 IOSTANDARD LVDS} [get_ports buffer_LVDS1_pem_n]
set_property -dict {PACKAGE_PIN M11 IOSTANDARD LVDS} [get_ports buffer_LVDS2_pwdn_p]
set_property -dict {PACKAGE_PIN L11 IOSTANDARD LVDS} [get_ports buffer_LVDS2_pwdn_n]

#FIXME
# set_property -dict {PACKAGE_PIN Y30 IOSTANDARD LVDS} [get_ports buffer_LVDS2_pem_p]
set_property -dict {IOSTANDARD LVDS} [get_ports buffer_LVDS2_pem_p]
# set_property -dict {PACKAGE_PIN AA30 IOSTANDARD LVDS} [get_ports buffer_LVDS2_pem_n]
set_property -dict {IOSTANDARD LVDS} [get_ports buffer_LVDS2_pem_n]