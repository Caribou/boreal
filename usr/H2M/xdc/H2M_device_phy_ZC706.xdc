#set_property DIFF_TERM true [get_ports ]
#set_property -dict {PACKAGE_PIN    IOSTANDARD LVCMOS25  } [get_ports {}]
#set_property -dict {PACKAGE_PIN    IOSTANDARD LVDS_25  } [get_ports {}]

## Clocks input
# SI5345_CLK_OUT8_{p,n}, HPC FMC {H4, H5}
set_property -dict {PACKAGE_PIN AE22 IOSTANDARD LVDS_25} [get_ports si_clk_p]
set_property -dict {PACKAGE_PIN AF22 IOSTANDARD LVDS_25} [get_ports si_clk_n]
set_property DIFF_TERM true [get_ports si_clk_*]

# FMC_REF_CLK_{p,n} HPC FMC {G21, G22}
# Reference clock to Si5345, in ch. 0 (output)
set_property PACKAGE_PIN U25 [get_ports fmc_ref_clk_p]
set_property PACKAGE_PIN V26 [get_ports fmc_ref_clk_n]
set_property IOSTANDARD LVDS_25 [get_ports fmc_ref_clk_*]
#set_property DIFF_TERM true     [get_ports fmc_ref_clk_*]

## TLU connections
# TLU_trig_{p,n} HPC FMC {D11, D12}
# Trigger (input)
set_property PACKAGE_PIN AH23 [get_ports TLU_trig_p]
set_property PACKAGE_PIN AH24 [get_ports TLU_trig_n]
set_property IOSTANDARD LVDS_25 [get_ports TLU_trig_*]
set_property DIFF_TERM true [get_ports TLU_trig_*]

# TLU_RST_{p,n} HPC FMC {D17, D18}
# T0 (input)
set_property PACKAGE_PIN AA22 [get_ports TLU_t0_p]
set_property PACKAGE_PIN AA23 [get_ports TLU_t0_n]
set_property IOSTANDARD LVDS_25 [get_ports TLU_t0_*]
set_property DIFF_TERM true [get_ports TLU_t0_*]

# TLU_BUSY_{p,n} HPC FMC {D14, D15}
# Detector busy flag (output)
set_property PACKAGE_PIN AD21 [get_ports TLU_busy_p]
set_property PACKAGE_PIN AE21 [get_ports TLU_busy_n]
set_property IOSTANDARD LVDS_25 [get_ports TLU_busy_*]
#set_property DIFF_TERM true [get_ports TLU_bsy_*]

# TLU_CLK_{p,n} HPC FMC {D8, D9}
# 40 MHz TLU clock (input)
set_property PACKAGE_PIN AG21 [get_ports TLU_clk_p]
set_property PACKAGE_PIN AH21 [get_ports TLU_clk_n]
set_property IOSTANDARD LVDS_25 [get_ports TLU_clk_*]
set_property DIFF_TERM true [get_ports TLU_clk_*]

# USER_SMA_CLOCK_P for gating
set_property -dict {PACKAGE_PIN AD18 IOSTANDARD LVCMOS25} [get_ports USER_SMA_CLOCK_P]
# debug SMA pin
set_property -dict {PACKAGE_PIN AD19 IOSTANDARD LVCMOS25 SLEW FAST} [get_ports USER_SMA_CLOCK_N]

## H2M chip connections
## FPGA -> chip
set_property -dict {PACKAGE_PIN Y22 IOSTANDARD LVDS_25} [get_ports chip_DAQ_CLK_IN_p]
set_property -dict {PACKAGE_PIN Y23 IOSTANDARD LVDS_25} [get_ports chip_DAQ_CLK_IN_n]
set_property -dict {PACKAGE_PIN V23 IOSTANDARD LVDS_25} [get_ports chip_SHUTTER_p]
set_property -dict {PACKAGE_PIN W24 IOSTANDARD LVDS_25} [get_ports chip_SHUTTER_n]
set_property -dict {PACKAGE_PIN AA24 IOSTANDARD LVDS_25} [get_ports chip_RESET_p]
set_property -dict {PACKAGE_PIN AB24 IOSTANDARD LVDS_25} [get_ports chip_RESET_n]
set_property -dict {PACKAGE_PIN AF20 IOSTANDARD LVDS_25} [get_ports chip_SC_CLK_IN_p]
set_property -dict {PACKAGE_PIN AG20 IOSTANDARD LVDS_25} [get_ports chip_SC_CLK_IN_n]
set_property -dict {PACKAGE_PIN AC24 IOSTANDARD LVDS_25} [get_ports chip_SC_DATA_IN_p]
set_property -dict {PACKAGE_PIN AD24 IOSTANDARD LVDS_25} [get_ports chip_SC_DATA_IN_n]
## chip -> FPGA
set_property -dict {PACKAGE_PIN AD23 IOSTANDARD LVDS_25} [get_ports chip_SC_CLK_OUT_p]
set_property -dict {PACKAGE_PIN AE23 IOSTANDARD LVDS_25} [get_ports chip_SC_CLK_OUT_n]
set_property DIFF_TERM true [get_ports chip_SC_CLK_OUT_*]
set_property -dict {PACKAGE_PIN AH19 IOSTANDARD LVDS_25} [get_ports chip_SC_DATA_OUT_p]
set_property -dict {PACKAGE_PIN AJ19 IOSTANDARD LVDS_25} [get_ports chip_SC_DATA_OUT_n]
set_property DIFF_TERM true [get_ports chip_SC_DATA_OUT_*]
set_property -dict {PACKAGE_PIN AJ20 IOSTANDARD LVDS_25} [get_ports chip_DIGITAL_TEST_OUT_p]
set_property -dict {PACKAGE_PIN AK20 IOSTANDARD LVDS_25} [get_ports chip_DIGITAL_TEST_OUT_n]
set_property DIFF_TERM true [get_ports chip_DIGITAL_TEST_OUT*]

## H2M chip board buffers
set_property -dict {PACKAGE_PIN Y26 IOSTANDARD LVDS_25} [get_ports buffer_LVDS1_pwdn_p]
set_property -dict {PACKAGE_PIN Y27 IOSTANDARD LVDS_25} [get_ports buffer_LVDS1_pwdn_n]
set_property -dict {PACKAGE_PIN P30 IOSTANDARD LVDS_25} [get_ports buffer_LVDS1_pem_p]
set_property -dict {PACKAGE_PIN R30 IOSTANDARD LVDS_25} [get_ports buffer_LVDS1_pem_n]
set_property -dict {PACKAGE_PIN T29 IOSTANDARD LVDS_25} [get_ports buffer_LVDS2_pwdn_p]
set_property -dict {PACKAGE_PIN U29 IOSTANDARD LVDS_25} [get_ports buffer_LVDS2_pwdn_n]
set_property -dict {PACKAGE_PIN Y30 IOSTANDARD LVDS_25} [get_ports buffer_LVDS2_pem_p]
set_property -dict {PACKAGE_PIN AA30 IOSTANDARD LVDS_25} [get_ports buffer_LVDS2_pem_n]