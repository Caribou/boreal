create_clock -period 25.000 -name chip_SC_CLK_OUT_p -waveform {0.000 12.500} [get_ports chip_SC_CLK_OUT_p]
create_clock -period 10.000 -name si_clk_p -waveform {0.000 5.000} [get_ports si_clk_p]
#create_clock -period 10.000 -name daq_clk_p -waveform {0.000 5.000} [get_ports daq_clk_p]

set_clock_groups -asynchronous -group [get_clocks {chip_SC_CLK_OUT_p}] -group [get_clocks {sc_i_clk}] -group [get_clocks {si_clk_p}] -group [get_clocks {clk_fpga_0}]