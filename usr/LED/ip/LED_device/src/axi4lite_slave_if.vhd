-- vhdl-linter-disable type-resolved
----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Younes Otarid
--
-- Create Date: 11/09/2023 11:57:34 AM
-- Design Name:
-- Module Name: axi4_slave_if - Behavioral
-- Project Name: Caribou
-- Target Devices: ZC706
-- Tool Versions:
-- Description: AXI4Lite Slave Interface
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- This RTL is based on the RTL generated using the AXI4 Peripheral packaging workflow
-- using the "Create and Package new IP" Vivado tool
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity axi4lite_slave_if is
  generic
  (
    -- Users to add parameters here

    -- User parameters ends
    -- Do not modify the parameters beyond this line

    -- Width of S_AXI data bus
    C_S_AXI_DATA_WIDTH : integer := 32;
    -- Width of S_AXI address bus
    C_S_AXI_ADDR_WIDTH : integer := 32
  );
  port
  (
    -- Users to add ports here

    -- Read interface
    axi_mem_rd_data_i   : in std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
    axi_mem_rd_addr_o   : out std_logic_vector(C_S_AXI_ADDR_WIDTH - (integer(ceil(log2(real(C_S_AXI_DATA_WIDTH)))) - 3) - 1 downto 0);
    axi_mem_rd_strobe_o : out std_logic;
    -- Write interface
    axi_mem_wr_data_o        : out std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
    axi_mem_wr_addr_o        : out std_logic_vector(C_S_AXI_ADDR_WIDTH - (integer(ceil(log2(real(C_S_AXI_DATA_WIDTH)))) - 3) - 1 downto 0);
    axi_mem_wr_byte_strobe_o : out std_logic_vector((C_S_AXI_DATA_WIDTH/8) - 1 downto 0);

    -- User ports ends
    -- Do not modify the ports beyond this line

    -- Global Clock Signal
    s_axi_aclk : in std_logic;
    -- Global Reset Signal. This Signal is Active LOW
    s_axi_aresetn : in std_logic;
    -- Write address (issued by master, acceped by Slave)
    s_axi_awaddr : in std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
    -- Write channel Protection type. This signal indicates the
    -- privilege and security level of the transaction, and whether
    -- the transaction is a data access or an instruction access.
    s_axi_awprot : in std_logic_vector(2 downto 0);
    -- Write address valid. This signal indicates that the master signaling
    -- valid write address and control information.
    s_axi_awvalid : in std_logic;
    -- Write address ready. This signal indicates that the slave is ready
    -- to accept an address and associated control signals.
    s_axi_awready : out std_logic;
    -- Write data (issued by master, acceped by Slave)
    s_axi_wdata : in std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
    -- Write strobes. This signal indicates which byte lanes hold
    -- valid data. There is one write strobe bit for each eight
    -- bits of the write data bus.
    s_axi_wstrb : in std_logic_vector(((C_S_AXI_DATA_WIDTH - 1)/8) downto 0);
    -- Write valid. This signal indicates that valid write
    -- data and strobes are available.
    s_axi_wvalid : in std_logic;
    -- Write ready. This signal indicates that the slave
    -- can accept the write data.
    s_axi_wready : out std_logic;
    -- Write response. This signal indicates the status
    -- of the write transaction.
    s_axi_bresp : out std_logic_vector(1 downto 0);
    -- Write response valid. This signal indicates that the channel
    -- is signaling a valid write response.
    s_axi_bvalid : out std_logic;
    -- Response ready. This signal indicates that the master
    -- can accept a write response.
    s_axi_bready : in std_logic;
    -- Read address (issued by master, acceped by Slave)
    s_axi_araddr : in std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
    -- Protection type. This signal indicates the privilege
    -- and security level of the transaction, and whether the
    -- transaction is a data access or an instruction access.
    s_axi_arprot : in std_logic_vector(2 downto 0);
    -- Read address valid. This signal indicates that the channel
    -- is signaling valid read address and control information.
    s_axi_arvalid : in std_logic;
    -- Read address ready. This signal indicates that the slave is
    -- ready to accept an address and associated control signals.
    s_axi_arready : out std_logic;
    -- Read data (issued by slave)
    s_axi_rdata : out std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
    -- Read response. This signal indicates the status of the
    -- read transfer.
    s_axi_rresp : out std_logic_vector(1 downto 0);
    -- Read valid. This signal indicates that the channel is
    -- signaling the required read data.
    s_axi_rvalid : out std_logic;
    -- Read ready. This signal indicates that the master can
    -- accept the read data and response information.
    s_axi_rready : in std_logic
  );
end axi4lite_slave_if;

architecture behavioral of axi4lite_slave_if is

  -- AXI4LITE signals
  signal axi_awaddr_s  : std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
  signal axi_awready_s : std_logic;
  signal axi_wready_s  : std_logic;
  signal axi_bresp_s   : std_logic_vector(1 downto 0);
  signal axi_bvalid_s  : std_logic;
  signal axi_araddr_s  : std_logic_vector(C_S_AXI_ADDR_WIDTH - 1 downto 0);
  signal axi_arready_s : std_logic;
  signal axi_rdata_s   : std_logic_vector(C_S_AXI_DATA_WIDTH - 1 downto 0);
  signal axi_rresp_s   : std_logic_vector(1 downto 0);
  signal axi_rvalid_s  : std_logic;

  -- Example-specific design signals
  -- local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
  -- ADDR_LSB is used for addressing 32/64 bit registers/memories
  -- ADDR_LSB = 2 for 32 bits (n downto 2)
  -- ADDR_LSB = 3 for 64 bits (n downto 3)
  constant ADDR_LSB : integer := (integer(ceil(log2(real(C_S_AXI_DATA_WIDTH)))) - 3);
  -- constant OPT_MEM_ADDR_BITS : integer := C_S_AXI_ADDR_WIDTH - ADDR_LSB;
  ------------------------------------------------
  ---- Signals for user logic register space example
  --------------------------------------------------
  signal slv_reg_rden_s : std_logic;
  signal slv_reg_wren_s : std_logic;
  signal aw_en_s        : std_logic;

begin
  -- I/O Connections assignments

  s_axi_awready <= axi_awready_s;
  s_axi_wready  <= axi_wready_s;
  s_axi_bresp   <= axi_bresp_s;
  s_axi_bvalid  <= axi_bvalid_s;
  s_axi_arready <= axi_arready_s;
  s_axi_rdata   <= axi_rdata_s;
  s_axi_rresp   <= axi_rresp_s;
  s_axi_rvalid  <= axi_rvalid_s;
  -- Implement axi_awready generation
  -- axi_awready is asserted for one S_AXI_ACLK clock cycle when both
  -- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
  -- de-asserted when reset is low.

  process (s_axi_aclk)
  begin
    if rising_edge(s_axi_aclk) then
      if s_axi_aresetn = '0' then
        axi_awready_s <= '0';
        aw_en_s       <= '1';
        else
        if (axi_awready_s = '0' and s_axi_awvalid = '1' and s_axi_wvalid = '1' and aw_en_s = '1') then
          -- slave is ready to accept write address when
          -- there is a valid write address and write data
          -- on the write address and data bus. This design
          -- expects no outstanding transactions.
          axi_awready_s <= '1';
          aw_en_s       <= '0';
          elsif (s_axi_bready = '1' and axi_bvalid_s = '1') then
          aw_en_s       <= '1';
          axi_awready_s <= '0';
          else
          axi_awready_s <= '0';
        end if;
      end if;
    end if;
  end process;

  -- Implement axi_awaddr latching
  -- This process is used to latch the address when both
  -- S_AXI_AWVALID and S_AXI_WVALID are valid.

  process (s_axi_aclk)
  begin
    if rising_edge(s_axi_aclk) then
      if s_axi_aresetn = '0' then
        axi_awaddr_s <= (others => '0');
        else
        if (axi_awready_s = '0' and s_axi_awvalid = '1' and s_axi_wvalid = '1' and aw_en_s = '1') then
          -- Write Address latching
          axi_awaddr_s <= s_axi_awaddr;
        end if;
      end if;
    end if;
  end process;

  -- Implement axi_wready generation
  -- axi_wready is asserted for one S_AXI_ACLK clock cycle when both
  -- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is
  -- de-asserted when reset is low.

  process (s_axi_aclk)
  begin
    if rising_edge(s_axi_aclk) then
      if s_axi_aresetn = '0' then
        axi_wready_s <= '0';
        else
        if (axi_wready_s = '0' and s_axi_wvalid = '1' and s_axi_awvalid = '1' and aw_en_s = '1') then
          -- slave is ready to accept write data when
          -- there is a valid write address and write data
          -- on the write address and data bus. This design
          -- expects no outstanding transactions.
          axi_wready_s <= '1';
          else
          axi_wready_s <= '0';
        end if;
      end if;
    end if;
  end process;

  -- Implement memory mapped register select and write logic generation
  -- The write data is accepted and written to memory mapped registers when
  -- axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
  -- select byte enables of slave registers while writing.
  -- These registers are cleared when reset (active low) is applied.
  -- Slave register write enable is asserted when valid address and data are available
  -- and the slave is ready to accept the write address and write data.
  slv_reg_wren_s <= axi_wready_s and s_axi_wvalid and axi_awready_s and s_axi_awvalid;

  axi_mem_wr_addr_o <= axi_awaddr_s(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB);

  gen_wr_strobe : for BYTE_IDX in 0 to ((C_S_AXI_DATA_WIDTH - 1)/8) generate
    process (s_axi_wstrb, slv_reg_wren_s)
    begin
      axi_mem_wr_byte_strobe_o(BYTE_IDX) <= slv_reg_wren_s and s_axi_wstrb(BYTE_IDX);
    end process;
  end generate;

  axi_mem_wr_data_o <= s_axi_wdata;

  -- Implement write response logic generation
  -- The write response and response valid signals are asserted by the slave
  -- when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.
  -- This marks the acceptance of address and indicates the status of
  -- write transaction.

  process (s_axi_aclk)
  begin
    if rising_edge(s_axi_aclk) then
      if s_axi_aresetn = '0' then
        axi_bvalid_s <= '0';
        axi_bresp_s  <= "00"; --need to work more on the responses
        else
        if (axi_awready_s = '1' and s_axi_awvalid = '1' and axi_wready_s = '1' and s_axi_wvalid = '1' and axi_bvalid_s = '0') then
          axi_bvalid_s <= '1';
          axi_bresp_s  <= "00";
          elsif (s_axi_bready = '1' and axi_bvalid_s = '1') then --check if bready is asserted while bvalid is high)
          axi_bvalid_s <= '0'; -- (there is a possibility that bready is always asserted high)
        end if;
      end if;
    end if;
  end process;

  -- Implement axi_arready generation
  -- axi_arready is asserted for one S_AXI_ACLK clock cycle when
  -- S_AXI_ARVALID is asserted. axi_awready is
  -- de-asserted when reset (active low) is asserted.
  -- The read address is also latched when S_AXI_ARVALID is
  -- asserted. axi_araddr is reset to zero on reset assertion.

  process (s_axi_aclk)
  begin
    if rising_edge(s_axi_aclk) then
      if s_axi_aresetn = '0' then
        axi_arready_s <= '0';
        axi_araddr_s  <= (others => '1');
        else
        if (axi_arready_s = '0' and s_axi_arvalid = '1') then
          -- indicates that the slave has acceped the valid read address
          axi_arready_s <= '1';
          -- Read Address latching
          axi_araddr_s <= s_axi_araddr;
          else
          axi_arready_s <= '0';
        end if;
      end if;
    end if;
  end process;

  -- Implement axi_arvalid generation
  -- axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both
  -- S_AXI_ARVALID and axi_arready are asserted. The slave registers
  -- data are available on the axi_rdata bus at this instance. The
  -- assertion of axi_rvalid marks the validity of read data on the
  -- bus and axi_rresp indicates the status of read transaction.axi_rvalid
  -- is deasserted on reset (active low). axi_rresp and axi_rdata are
  -- cleared to zero on reset (active low).
  process (s_axi_aclk)
  begin
    if rising_edge(s_axi_aclk) then
      if s_axi_aresetn = '0' then
        axi_rvalid_s <= '0';
        axi_rresp_s  <= "00";
        else
        if (axi_arready_s = '1' and s_axi_arvalid = '1' and axi_rvalid_s = '0') then
          -- Valid read data is available at the read data bus
          axi_rvalid_s <= '1';
          axi_rresp_s  <= "00"; -- 'OKAY' response
          elsif (axi_rvalid_s = '1' and s_axi_rready = '1') then
          -- Read data is accepted by the master
          axi_rvalid_s <= '0';
        end if;
      end if;
    end if;
  end process;

  -- Implement memory mapped register select and read logic generation
  -- Slave register read enable is asserted when valid address is available
  -- and the slave is ready to accept the read address.
  slv_reg_rden_s <= axi_arready_s and s_axi_arvalid and (not axi_rvalid_s);

  -- Output register or memory read data
  process (s_axi_aclk) is
  begin
    if (rising_edge (s_axi_aclk)) then
      if (s_axi_aresetn = '0') then
        axi_rdata_s <= (others => '0');
        else
        if (slv_reg_rden_s = '1') then
          -- When there is a valid read address (S_AXI_ARVALID) with
          -- acceptance of read address by the slave (axi_arready),
          -- output the read dada
          -- Read address mux
          axi_rdata_s <= axi_mem_rd_data_i; -- register read data
        end if;
      end if;
    end if;
  end process;
  -- Add user logic here
  axi_mem_rd_strobe_o <= slv_reg_rden_s;
  axi_mem_rd_addr_o   <= axi_araddr_s(C_S_AXI_ADDR_WIDTH - 1 downto ADDR_LSB);
  -- User logic ends

end behavioral;
