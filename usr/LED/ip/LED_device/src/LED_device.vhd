----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Younes Otarid (younes.otarid@cern.ch)
--
-- Create Date: 11/28/2023 10:30:55 AM
-- Design Name:
-- Module Name: LED_device - behavioral
-- Project Name: Caribou
-- Target Devices: ZC706
-- Tool Versions:
-- Description: LED blinking control block
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;
use work.reg_map_utils.all;

entity LED_device is
  generic (
    -- Width of S_AXI data bus
    AXI_DATA_WIDTH : integer := 32;
    -- Width of S_AXI address bus
    AXI_ADDR_WIDTH : integer := 32;
    -- Number of LEDs
    N_LED : integer := 4;
    -- Clock divider value
    CLK_DIV : natural := 100000000
  );
  port (
    -- Clock signal
    clk_i : in std_logic;
    -- Reset signal
    arst_n_i : in std_logic;
    -- LED driving signal
    led_o : out std_logic_vector(N_LED - 1 downto 0);

    -- AXI4Lite slave interface
    s_axi_aclk    : in std_logic;
    s_axi_aresetn : in std_logic;
    s_axi_awaddr  : in std_logic_vector(AXI_ADDR_WIDTH - 1 downto 0);
    s_axi_awprot  : in std_logic_vector(2 downto 0);
    s_axi_awvalid : in std_logic;
    s_axi_awready : out std_logic;
    s_axi_wdata   : in std_logic_vector(AXI_DATA_WIDTH - 1 downto 0);
    s_axi_wstrb   : in std_logic_vector(((AXI_DATA_WIDTH - 1)/8) downto 0);
    s_axi_wvalid  : in std_logic;
    s_axi_wready  : out std_logic;
    s_axi_bresp   : out std_logic_vector(1 downto 0);
    s_axi_bvalid  : out std_logic;
    s_axi_bready  : in std_logic;
    s_axi_araddr  : in std_logic_vector(AXI_ADDR_WIDTH - 1 downto 0);
    s_axi_arprot  : in std_logic_vector(2 downto 0);
    s_axi_arvalid : in std_logic;
    s_axi_arready : out std_logic;
    s_axi_rdata   : out std_logic_vector(AXI_DATA_WIDTH - 1 downto 0);
    s_axi_rresp   : out std_logic_vector(1 downto 0);
    s_axi_rvalid  : out std_logic;
    s_axi_rready  : in std_logic
  );
end LED_device;

architecture behavioral of LED_device is

  constant REG_N : integer := 2;
  -- LED drive enable register
  constant ENABLE_REG  : integer := 0;
  constant CLK_DIV_REG : integer := 1;

  -- Register map data out
  signal data_in_s  : array_2d(REG_N - 1 downto 0)(AXI_DATA_WIDTH - 1 downto 0);
  signal data_out_s : array_2d(REG_N - 1 downto 0)(AXI_DATA_WIDTH - 1 downto 0);

  -- Drive enable signal
  signal enable_s : std_logic;
  -- LED drive signal
  signal drive_s : std_logic;
  -- Clock counter
  signal clk_cntr_s : natural range 0 to CLK_DIV;

begin

  -- Register map interface instantiation
  reg_map_inst : entity work.reg_map
    generic map(
      AXI_DATA_WIDTH => AXI_DATA_WIDTH,
      AXI_ADDR_WIDTH => AXI_ADDR_WIDTH,
      REG_N          => REG_N
    )
    port map
    (
      -- Register data IN
      data_in_i => data_in_s,
      -- Register data OUT
      data_out_o => data_out_s,
      -- AXI4Lite interface
      s_axi_aclk    => s_axi_aclk,
      s_axi_aresetn => s_axi_aresetn,
      s_axi_awaddr  => s_axi_awaddr,
      s_axi_awprot  => s_axi_awprot,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_awready => s_axi_awready,
      s_axi_wdata   => s_axi_wdata,
      s_axi_wstrb   => s_axi_wstrb,
      s_axi_wvalid  => s_axi_wvalid,
      s_axi_wready  => s_axi_wready,
      s_axi_bresp   => s_axi_bresp,
      s_axi_bvalid  => s_axi_bvalid,
      s_axi_bready  => s_axi_bready,
      s_axi_araddr  => s_axi_araddr,
      s_axi_arprot  => s_axi_arprot,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_arready => s_axi_arready,
      s_axi_rdata   => s_axi_rdata,
      s_axi_rresp   => s_axi_rresp,
      s_axi_rvalid  => s_axi_rvalid,
      s_axi_rready  => s_axi_rready
    );

  enable_s <= data_out_s(ENABLE_REG)(0);

  led_drive_generate : for I in 0 to N_LED - 2 generate
    led_o(I) <= drive_s when (enable_s = '1' and (i mod 2 = 0)) else
    not(drive_s) when (enable_s = '1' and (i mod 2 = 1)) else
    '0';
  end generate led_drive_generate;
  led_o(N_LED - 1) <= not(enable_s);

  -- LED driving process to blink
  blink_proc : process (clk_i)
  begin
    if (rising_edge(clk_i)) then
      if (not(arst_n_i) = '1') then
        drive_s    <= '0';
        clk_cntr_s <= 0;
      else
        if clk_cntr_s = CLK_DIV - 1 then
          drive_s    <= not(drive_s);
          clk_cntr_s <= 0;
        else
          clk_cntr_s <= clk_cntr_s + 1;
        end if;
      end if;
    end if;
  end process blink_proc;

end behavioral;
