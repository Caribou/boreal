----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Younes Otarid
--
-- Create Date: 11/09/2023 01:15:07 PM
-- Design Name:
-- Module Name: reg_map_utils - Behavioral
-- Project Name: Caribou
-- Target Devices:
-- Tool Versions:
-- Description: Common utility functions or records to be used accross the design
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

package reg_map_utils is
  --! 2D array of std_logic_vector
  type array_2d is array (integer range <>) of std_logic_vector;

  --! Function to calculate the ceiling of the logarithm base 2 of x
  function clog2(value : natural) return natural;

end package reg_map_utils;

package body reg_map_utils is

  --! Custom clog2 implementation
  function clog2(value : natural) return natural is
  begin
    if value = 1 then
      return 1;
    else
      return integer(ceil(log2(real(value))));
    end if;
  end;

end package body reg_map_utils;
