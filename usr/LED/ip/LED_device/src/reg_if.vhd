-- vhdl-linter-disable type-resolved
----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Younes Otarid
--
-- Create Date: 11/09/2023 06:09:23 PM
-- Design Name:
-- Module Name: reg_if - behavioral
-- Project Name: Caribou
-- Target Devices: ZC706
-- Tool Versions:
-- Description: AXI memory and registers interface
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.reg_map_utils.all;

entity reg_if is
  generic (
    -- Number of registers
    REG_N : integer := 16;
    -- Width of data bus
    DATA_WIDTH : integer := 32
  );
  port (
    -- Register interface
    -- Write
    reg_wr_data_o        : out std_logic_vector(DATA_WIDTH - 1 downto 0);
    reg_wr_byte_strobe_o : out array_2d(REG_N - 1 downto 0)((DATA_WIDTH - 1)/8 downto 0);
    -- Read
    reg_rd_data_i   : in array_2d(REG_N - 1 downto 0)(DATA_WIDTH - 1 downto 0);
    reg_rd_strobe_o : out std_logic_vector(REG_N - 1 downto 0);

    -- Memory interface
    -- Write
    mem_wr_select_i      : in std_logic;
    mem_wr_addr_i        : in std_logic_vector(clog2(REG_N) - 1 downto 0);
    mem_wr_byte_strobe_i : in std_logic_vector((DATA_WIDTH - 1)/8 downto 0);
    mem_wr_data_i        : in std_logic_vector(DATA_WIDTH - 1 downto 0);
    -- Read
    mem_rd_select_i : in std_logic;
    mem_rd_data_o   : out std_logic_vector(DATA_WIDTH - 1 downto 0);
    mem_rd_addr_i   : in std_logic_vector(clog2(REG_N) - 1 downto 0);
    mem_rd_strobe_i : in std_logic
  );
end reg_if;

architecture behavioral of reg_if is

  signal mem_rd_dout_s   : std_logic_vector(DATA_WIDTH - 1 downto 0);
  signal reg_rd_strobe_s : std_logic_vector(REG_N - 1 downto 0);
  signal reg_wr_select_s : std_logic_vector(REG_N - 1 downto 0);

begin

  -- Read and Write transactions data external forwarding
  reg_wr_data_o <= mem_wr_data_i;
  mem_rd_data_o <= mem_rd_dout_s;

  -- Read transaction data internal forwarding
  mem_rd_data : process (mem_rd_select_i, mem_rd_addr_i, reg_rd_data_i)
  begin
    if mem_rd_select_i = '1' and (to_integer(unsigned(mem_rd_addr_i)) < REG_N) then
      mem_rd_dout_s <= reg_rd_data_i(to_integer(unsigned(mem_rd_addr_i)));
      else
      mem_rd_dout_s <= (others => '0');
    end if;
  end process;

  -- Read transaction demultiplexing
  gen_rd_demux : for I in 0 to REG_N - 1 generate
    mem_rd_strobe : process (mem_rd_select_i, mem_rd_addr_i, mem_rd_strobe_i)
    begin
      if mem_rd_select_i = '1' and (to_integer(unsigned(mem_rd_addr_i)) = I) then
        reg_rd_strobe_s(I) <= mem_rd_strobe_i;
        else
        reg_rd_strobe_s(I) <= '0';
      end if;
    end process;
    reg_rd_strobe_o(I) <= reg_rd_strobe_s(I);
  end generate;

  -- Write transaction demultiplexing
  gen_wr_demux : for I in 0 to REG_N - 1 generate
    mem_wr : process (mem_wr_select_i, mem_wr_addr_i, mem_wr_byte_strobe_i)
    begin
      if mem_wr_select_i = '1' and (to_integer(unsigned(mem_wr_addr_i)) = I) then
        reg_wr_select_s(I) <= '1';
        else
        reg_wr_select_s(I) <= '0';
      end if;
    end process;
    reg_wr_byte_strobe_o(I) <= mem_wr_byte_strobe_i when reg_wr_select_s(I) = '1' else
    (others => '0');
  end generate;

end behavioral;
