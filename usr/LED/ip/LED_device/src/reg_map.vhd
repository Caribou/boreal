----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Younes Otarid
--
-- Create Date: 11/24/2023 01:33:24 PM
-- Design Name:
-- Module Name: reg_map - behavioral
-- Project Name: Caribou
-- Target Devices: ZC706
-- Tool Versions:
-- Description: AXI register map interface
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.std_logic_misc.all;
use work.reg_map_utils.all;

entity reg_map is
  generic (
    -- Width of S_AXI data bus
    AXI_DATA_WIDTH : integer := 32;
    -- Width of S_AXI address bus
    AXI_ADDR_WIDTH : integer := 32;
    -- Number of registers
    REG_N : integer := 16
  );
  port (
    -- Register data IN
    data_in_i : in array_2d(REG_N - 1 downto 0)(AXI_DATA_WIDTH - 1 downto 0);
    -- Register data OUT
    data_out_o : out array_2d(REG_N - 1 downto 0)(AXI_DATA_WIDTH - 1 downto 0);

    -- AXI4Lite slave interface
    s_axi_aclk    : in std_logic;
    s_axi_aresetn : in std_logic;
    s_axi_awaddr  : in std_logic_vector(AXI_ADDR_WIDTH - 1 downto 0);
    s_axi_awprot  : in std_logic_vector(2 downto 0);
    s_axi_awvalid : in std_logic;
    s_axi_awready : out std_logic;
    s_axi_wdata   : in std_logic_vector(AXI_DATA_WIDTH - 1 downto 0);
    s_axi_wstrb   : in std_logic_vector(((AXI_DATA_WIDTH - 1)/8) downto 0);
    s_axi_wvalid  : in std_logic;
    s_axi_wready  : out std_logic;
    s_axi_bresp   : out std_logic_vector(1 downto 0);
    s_axi_bvalid  : out std_logic;
    s_axi_bready  : in std_logic;
    s_axi_araddr  : in std_logic_vector(AXI_ADDR_WIDTH - 1 downto 0);
    s_axi_arprot  : in std_logic_vector(2 downto 0);
    s_axi_arvalid : in std_logic;
    s_axi_arready : out std_logic;
    s_axi_rdata   : out std_logic_vector(AXI_DATA_WIDTH - 1 downto 0);
    s_axi_rresp   : out std_logic_vector(1 downto 0);
    s_axi_rvalid  : out std_logic;
    s_axi_rready  : in std_logic
  );
end reg_map;

architecture behavioral of reg_map is

  -- Constants
  constant AXI_DATA_BYTES     : integer := (AXI_DATA_WIDTH / 8);
  constant AXI_ADDR_LSB       : integer := integer(ceil(log2(real(AXI_DATA_WIDTH)))) - 3;
  constant AXI_REG_ADDR_WIDTH : integer := AXI_ADDR_WIDTH - AXI_ADDR_LSB;

  -- AXI4Lite Read interface signals
  signal axi_mem_rd_data_s   : std_logic_vector(AXI_DATA_WIDTH - 1 downto 0);
  signal axi_mem_rd_addr_s   : std_logic_vector(AXI_REG_ADDR_WIDTH - 1 downto 0);
  signal axi_mem_rd_strobe_s : std_logic;
  -- AXI4Lite Write interface signals
  signal axi_mem_wr_data_s        : std_logic_vector(AXI_DATA_WIDTH - 1 downto 0);
  signal axi_mem_wr_addr_s        : std_logic_vector(AXI_REG_ADDR_WIDTH - 1 downto 0);
  signal axi_mem_wr_byte_strobe_s : std_logic_vector(AXI_DATA_BYTES - 1 downto 0);

  -- Register interface signals
  signal reg_wr_data_s        : std_logic_vector(AXI_DATA_WIDTH - 1 downto 0);
  signal reg_wr_byte_strobe_s : array_2d(REG_N - 1 downto 0)(AXI_DATA_BYTES - 1 downto 0);
  signal reg_rd_strobe_s      : std_logic_vector(REG_N - 1 downto 0);
  signal reg_rd_data_s        : array_2d(REG_N - 1 downto 0)(AXI_DATA_WIDTH - 1 downto 0);
  -- Memory interface signals
  signal mem_wr_select_s : std_logic;
  signal mem_rd_select_s : std_logic;

begin

  -- AXI4Lite slave interface instantiation
  axi4lite_slave_if_inst : entity work.axi4lite_slave_if
    generic map(
      C_S_AXI_DATA_WIDTH => AXI_DATA_WIDTH,
      C_S_AXI_ADDR_WIDTH => AXI_ADDR_WIDTH
    )
    port map
    (
      -- Memory read interface
      axi_mem_rd_data_i   => axi_mem_rd_data_s,
      axi_mem_rd_addr_o   => axi_mem_rd_addr_s,
      axi_mem_rd_strobe_o => axi_mem_rd_strobe_s,
      -- Memory write interface
      axi_mem_wr_data_o        => axi_mem_wr_data_s,
      axi_mem_wr_addr_o        => axi_mem_wr_addr_s,
      axi_mem_wr_byte_strobe_o => axi_mem_wr_byte_strobe_s,
      -- AXI4Lite interface
      s_axi_aclk    => s_axi_aclk,
      s_axi_aresetn => s_axi_aresetn,
      s_axi_awaddr  => s_axi_awaddr,
      s_axi_awprot  => s_axi_awprot,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_awready => s_axi_awready,
      s_axi_wdata   => s_axi_wdata,
      s_axi_wstrb   => s_axi_wstrb,
      s_axi_wvalid  => s_axi_wvalid,
      s_axi_wready  => s_axi_wready,
      s_axi_bresp   => s_axi_bresp,
      s_axi_bvalid  => s_axi_bvalid,
      s_axi_bready  => s_axi_bready,
      s_axi_araddr  => s_axi_araddr,
      s_axi_arprot  => s_axi_arprot,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_arready => s_axi_arready,
      s_axi_rdata   => s_axi_rdata,
      s_axi_rresp   => s_axi_rresp,
      s_axi_rvalid  => s_axi_rvalid,
      s_axi_rready  => s_axi_rready

    );

  -- Register interface instantiation
  reg_if_inst : entity work.reg_if
    generic map(
      REG_N => REG_N
    )
    port map
    (
      -- Register interface
      reg_wr_data_o        => reg_wr_data_s,
      reg_wr_byte_strobe_o => reg_wr_byte_strobe_s,
      reg_rd_data_i        => reg_rd_data_s,
      reg_rd_strobe_o      => reg_rd_strobe_s,

      -- Memory interface
      mem_wr_select_i      => mem_wr_select_s,
      mem_rd_select_i      => mem_rd_select_s,
      mem_rd_addr_i        => axi_mem_rd_addr_s(clog2(REG_N) - 1 downto 0),
      mem_rd_data_o        => axi_mem_rd_data_s,
      mem_rd_strobe_i      => axi_mem_rd_strobe_s,
      mem_wr_data_i        => axi_mem_wr_data_s,
      mem_wr_addr_i        => axi_mem_wr_addr_s(clog2(REG_N) - 1 downto 0),
      mem_wr_byte_strobe_i => axi_mem_wr_byte_strobe_s
    );

  -- Select AXI read transactions when register is requested
  axi_rd_select : process (axi_mem_rd_addr_s)
  begin
    if (or_reduce(axi_mem_rd_addr_s(AXI_REG_ADDR_WIDTH - 1 downto clog2(REG_N))) = '1') then
      mem_rd_select_s <= '1';
      else
      mem_rd_select_s <= '0';
    end if;
  end process;

  -- Select AXI write transactions when register is requested
  axi_wr_select : process (axi_mem_wr_addr_s)
  begin
    if (or_reduce(axi_mem_wr_addr_s(AXI_REG_ADDR_WIDTH - 1 downto clog2(REG_N))) = '1') then
      mem_wr_select_s <= '1';
      else
      mem_wr_select_s <= '0';
    end if;
  end process;

  -- Register write data forwarding
  axi_wr_register_gen : for REG_IDX in 0 to REG_N - 1 generate
    axi_wr_register : process (s_axi_aclk)
    begin
      if (rising_edge(s_axi_aclk)) then
        if (not(s_axi_aresetn)) then
          data_out_o(REG_IDX) <= (others => '0');
          else
          -- Forward byte-1
          if (reg_wr_byte_strobe_s(REG_IDX)(0) = '1') then
            data_out_o(REG_IDX)(7 downto 0) <= reg_wr_data_s(7 downto 0);
          end if;
          -- Forward byte-2
          if (reg_wr_byte_strobe_s(REG_IDX)(1) = '1') then
            data_out_o(REG_IDX)(15 downto 8) <= reg_wr_data_s(15 downto 8);
          end if;
          -- Forward byte-3
          if (reg_wr_byte_strobe_s(REG_IDX)(0) = '1') then
            data_out_o(REG_IDX)(23 downto 16) <= reg_wr_data_s(23 downto 16);
          end if;
          -- Forward byte-4
          if (reg_wr_byte_strobe_s(REG_IDX)(0) = '1') then
            data_out_o(REG_IDX)(31 downto 24) <= reg_wr_data_s(31 downto 24);
          end if;
        end if;
      end if;
    end process;
  end generate;

  -- Register read data forwarding
  reg_rd_data_s <= data_in_i;

end behavioral;
