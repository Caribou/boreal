"""test_register_map_if.py"""

import cocotb
from cocotb.triggers import Timer, RisingEdge
from cocotb.clock import Clock
from cocotbext.axi import AxiLiteBus, AxiLiteMaster


@cocotb.coroutine
async def reset(dut):
    """DUT reset"""
    # Reset AXI4Lite interface
    dut.s_axi_aresetn.setimmediatevalue(1)
    await RisingEdge(dut.s_axi_aclk)
    await RisingEdge(dut.s_axi_aclk)
    dut.s_axi_aresetn.value = 0
    await RisingEdge(dut.s_axi_aclk)
    await RisingEdge(dut.s_axi_aclk)
    dut.s_axi_aresetn.value = 1

    # Reset DUT module
    dut.arst_n_i.setimmediatevalue(1)
    await RisingEdge(dut.clk_i)
    await RisingEdge(dut.clk_i)
    dut.arst_n_i.value = 0
    await RisingEdge(dut.clk_i)
    await RisingEdge(dut.clk_i)
    dut.arst_n_i.value = 1


@cocotb.coroutine
async def test_blink(dut):
    """Test AXI4Lite register read logic"""

    # Start concurrent clock generation
    cocotb.start_soon(Clock(dut.s_axi_aclk, 10, units="ns").start())
    cocotb.start_soon(Clock(dut.clk_i, 10, units="ns").start())

    # AXI4Lite master
    axi4lite_master = AxiLiteMaster(
        AxiLiteBus.from_prefix(dut, "s_axi"),
        dut.s_axi_aclk,
        dut.s_axi_aresetn,
        reset_active_level=False,
    )

    # Reset AXI4Lite slave interface and usr module
    await reset(dut)

    # write address and data
    base_addr = 0x43C10000
    addr_lsb = 2
    reg_idx = 0

    dut._log.info("")
    dut._log.info("*********************************************************")

    # Write 1 to DriveEnableReg to enable blinking
    addr = base_addr | (reg_idx << addr_lsb)
    data = 1
    # Write data
    dut._log.info("Writing data to register %s at address %s", reg_idx, hex(addr))
    cocotb.start_soon(axi4lite_master.write(addr, data.to_bytes(4, byteorder="little")))
    # Wait for data_out to settle
    await Timer(320, units="ns")
    # Check led blinking output
    assert dut.led_o[3].value == int(not data)

    # Write 1 to DriveEnableReg to disable blinking
    addr = base_addr | (reg_idx << addr_lsb)
    data = 0
    # Write data
    dut._log.info("Writing data to register %s at address %s", reg_idx, hex(addr))
    cocotb.start_soon(axi4lite_master.write(addr, data.to_bytes(4, byteorder="little")))
    # Wait for data_out to settle
    await Timer(320, units="ns")
    # Check led blinking output
    assert dut.led_o[3].value == int(not data)


@cocotb.test()
async def test_LED_device(dut):
    """Test LED blinking"""

    dut._log.info("##########################################################")
    dut._log.info("######################## BLINK TEST ######################")
    dut._log.info("##########################################################")
    await test_blink(dut)
