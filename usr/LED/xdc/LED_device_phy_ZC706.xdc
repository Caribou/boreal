#! /usr/bin/tclsh

# User LED
set_property -dict {PACKAGE_PIN Y21 IOSTANDARD LVCMOS25} [get_ports {led_o[0]}]
set_property -dict {PACKAGE_PIN G2 IOSTANDARD LVCMOS15} [get_ports {led_o[1]}]
set_property -dict {PACKAGE_PIN W21 IOSTANDARD LVCMOS25} [get_ports {led_o[2]}]
set_property -dict {PACKAGE_PIN A17 IOSTANDARD LVCMOS15} [get_ports {led_o[3]}]
