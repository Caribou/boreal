#! /usr/bin/tclsh

# User LED
set_output_delay -clock [get_clocks {clk_fpga_0}] 0 [get_ports {led_o[*]}]
set_false_path -to [get_ports {led_o[*]}]
