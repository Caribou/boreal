import os
import subprocess

from py.Color import Color as color

def save_tcl_commands(cmd_list, file):
    """Saving TCL commands in executable file"""
    os.system(f"rm -f {file}")
    for cmd in cmd_list:
        os.system(f"echo {cmd} >> {file}")

def run_tcl_commands(tcl_file, prj_file=""):
    """Execute TCL commands"""
    os.system(f"vivado -mode batch -nolog -nojournal -source {tcl_file} {prj_file}")


def run_shell_commands(cmd_list):
    """Run petalinux command"""
    for cmd in cmd_list:
        print(f"{color.BLUE} [RUN] {cmd} {color.END}")
        try:
            subprocess.run(cmd, shell=True, check=True)
        except subprocess.CalledProcessError as e:
            print(f"{color.RED} [ERROR] {e} {color.END}")
            return 1
    return 0
