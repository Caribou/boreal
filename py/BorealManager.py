import os
import re
from py.Color import Color as color
import py.Utils as utils

BOREAL_PATH = os.getcwd()

class BorealManager:

  DEVICE_SUPPORT_MAP = {}

  def __init__(self) -> None:
    self.DEVICE_SUPPORT_MAP = self.list()
  
  def _assert(self, device, board):
    """Assert if the device and board are supported"""
    assert device and board, "Board and device should be provided"
    assert device in self.DEVICE_SUPPORT_MAP.keys(), f"Project {device} not supported"
    assert board in self.DEVICE_SUPPORT_MAP[device], f"Board {board} not supported"
  
  # Project Configuration
  def list(self, show=False):
    """List available devices and target boards"""
    base_path = os.path.join(BOREAL_PATH, "usr")
    dev_support_map = {}
    for dev in os.listdir(base_path):
        dev_path = os.path.join(base_path, dev)
        
        # Ensure it's a directory
        if os.path.isdir(dev_path):
            dev_path = os.path.join(dev_path, "prj")
            
            if os.path.exists(dev_path) and os.path.isdir(dev_path):
                supported_boards = []
                
                for file in os.listdir(dev_path):
                    match = re.match(r".*_top_(\w+)_vivado\.tcl", file)
                    if match:
                        supported_boards.append(match.group(1).upper())  # Normalize to uppercase

                if supported_boards:
                    dev_support_map[dev] = supported_boards
    #print available devices
    if show:
      print(color.GREEN + color.BOLD + "Available devices:" + color.END)
      for device, boards in dev_support_map.items():
          print(color.YELLOW + f"\t{device}:" + color.END + f"{boards}" + color.END)

    #return available devices
    return dev_support_map

  def _get_prj_path(self, device):
    """Get the project path"""
    prj_path = os.path.join(BOREAL_PATH, f"usr/{device}/prj/")
    if not os.path.exists(prj_path):
      raise FileNotFoundError(f"Project path not found: {prj_path}")
    return prj_path
  
  def _get_prj_name(self, device, board):
    """Get the project name"""
    prj_name = f"{device}_device_top_{board}_vivado"
    return prj_name
  
  def _get_tcl_info(self, device, board):
    """Get the project tcl"""
    prj_name = self._get_prj_name(device, board)
    tcl_path = self._get_prj_path(device)
    tcl_file = os.path.join(tcl_path, f"create_{prj_name}.tcl")
    if not os.path.exists(tcl_file):
      raise FileNotFoundError(f"Project creation file not found: {tcl_file}")
    return tcl_path, tcl_file
  
  def _get_prj_info(self, device, board):
    """Get the project vivado"""
    prj_name = self._get_prj_name(device, board)
    prj_path = os.path.join(self._get_prj_path(device), prj_name)
    prj_file = os.path.join(prj_path, f"{prj_name}.xpr")
    if not os.path.exists(prj_file):
      raise FileNotFoundError(f"Project Vivado file not found: {prj_file}")
    return prj_name, prj_path, prj_file
  
  # Project Creation
  def create(self, device, board):
      """Creating the Vivado device"""
      #assert if device and board are valid
      self._assert(device, board)

      #get TCL info
      tcl_path, tcl_file = self._get_tcl_info(device, board)
      print(
          color.GREEN
          + color.BOLD
          + f"Creating the Vivado device for {device} on {board}"
          + color.END
      )
      #go to project folder and execute TCL file
      os.chdir(tcl_path)
      utils.run_tcl_commands(tcl_file)
  
  def _synth_prj(self, device, board):
    """Synthesize the project"""
    _, prj_path, prj_file = self._get_prj_info(device, board)
    print(
        color.GREEN
        + color.BOLD
        + f"Synthesizing the Vivado project for {device} on {board}"
        + color.END
    )
    # Go to Vivado device directory
    os.chdir(prj_path)
    # Prepare for TCL command container and file
    cmd_list = []
    cmd_list.append("reset_run synth_1")
    cmd_list.append("launch_runs synth_1 -jobs 6")
    cmd_list.append("wait_on_run synth_1")
    # Save TCL commands in executable file
    tcl_file = "synth.tcl"
    utils.save_tcl_commands(cmd_list, tcl_file)
    # Execute TCL commands
    utils.run_tcl_commands(tcl_file, prj_file)

  def _check_synth(self, device, board):
    """Check if synthesis was successful"""
    prj_name, prj_path, _ = self._get_prj_info(device, board)
    # Log path
    log_path = os.path.join(prj_path, f"{prj_name}.runs/synth_1/") 
    print(color.GREEN + color.BOLD + "Checking if synthesis was successful" + color.END)
    cmd = f'if grep -nr "ERROR:" {log_path}/runme.log; then exit 1; fi'
    status = utils.run_shell_commands([cmd])
    if status == 0:
        print(color.GREEN + color.BOLD + "Synthesis was successful" + color.END)
    else:
        print(color.RED + color.BOLD + "Synthesis failed" + color.END)
  
  def _impl_prj(self, device, board):
    """Implement the project"""
    prj_name, prj_path, prj_file = self._get_prj_info(device, board)
    print(
        color.GREEN
        + color.BOLD
        + f"Implementing the Vivado project for {device} on {board}"
        + color.END
    )
    # Go to Vivado device directory
    os.chdir(prj_path)
    # Prepare for TCL command container and file
    cmd_list = []
    cmd_list.append("reset_run impl_1")
    cmd_list.append("launch_runs impl_1 -to_step write_bitstream -jobs 6")
    cmd_list.append("wait_on_run impl_1")
    hw_file = os.path.join(prj_path, f"{prj_name}.runs/impl_1/{device}_device_top_{board}_wrapper.xsa")
    cmd_list.append(f"write_hw_platform -fixed -include_bit -force -file {hw_file}")
    # Save TCL commands in executable file
    tcl_file = "impl.tcl"
    utils.save_tcl_commands(cmd_list, tcl_file)
    # Execute TCL commands
    utils.run_tcl_commands(tcl_file, prj_file)

  def _check_impl(self, device, board):
    """Check if implementation was successful"""
    prj_name, prj_path, _ = self._get_prj_info(device, board)
    # Log path
    log_path = os.path.join(prj_path, f"{prj_name}.runs/impl_1/") 
    print(color.GREEN + color.BOLD + "Checking if implementation was successful" + color.END)
    cmd = f'if grep -nr "ERROR:" {log_path}/runme.log; then exit 1; fi'
    status = utils.run_shell_commands([cmd])
    if status == 0:
        print(color.GREEN + color.BOLD + "Implementation was successful" + color.END)
    else:
        print(color.RED + color.BOLD + "Implementation failed" + color.END)

  # Project Building
  def build(self, device, board):
    """Building the Vivado device"""
    #assert if device and board are valid, and at least one build option is enabled
    self._assert(device, board)
    
    # Synthesize project
    self._synth_prj(device, board)
    self._check_synth(device, board)

    # Implement project 
    self._impl_prj(device, board)
    self._check_impl(device, board)
  