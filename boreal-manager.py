#!/usr/bin/env python3

import os
import fileinput
import argparse
import subprocess
import re
import py.BorealManager as bm

# Main
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Boreal Vivado project manager")
    subparsers = parser.add_subparsers(dest="command", help="Sub-command help")

    # Global
    global_parser = argparse.ArgumentParser(add_help=False)
    global_parser.add_argument(
        "--device",
        metavar="device_name",
        help="Name of the device",
    )
    global_parser.add_argument(
        "--board",
        metavar="board_name",
        help="Name of the project",
    )

    list_parser = subparsers.add_parser("list", help="List available devices")

    # Creation
    create_parser = subparsers.add_parser("create", parents=[global_parser], help="Create Vivado project")

    # Building
    build_parser = subparsers.add_parser("build", parents=[global_parser], help="Build Vivado project")

    args = parser.parse_args()

    manager = bm.BorealManager()

    if not args.command:
        parser.print_help()

    # List available devices
    if args.command == "list":
        manager.list(True)

    # Create and/or configure the device
    if args.command == "create":
        # create Vivado project
        manager.create(args.device, args.board)

    # Build the device
    if args.command == "build":
        manager.build(args.device, args.board)

