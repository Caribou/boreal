"""Convenience script to ensure exiting with error code from simulation for CI"""

from xml.dom.minidom import parse

document = parse("results.xml")

fail = False

failure = document.getElementsByTagNameNS("*", "failure")
skipped = document.getElementsByTagNameNS("*", "skipped")


def print_testcase_results():
    for tc in document.getElementsByTagName("testcase"):
        if tc.hasChildNodes():
            print(f"{tc.childNodes[1].tagName}: {tc.attributes.items()[3][1]}")


if len(failure):
    print("There are failed tests:")
    print_testcase_results()
    exit(1)
if len(skipped):
    print("There are skipped tests:")
    print_testcase_results()
exit(0)
